
import java.util.*;
import java.lang.Math;

public class First
{
  public static void main(String[] args)
  {
    Dog sup = new Dog();
    sup.outputAge(); sup.outputBark();
  }
}

public class Dog
{
  String bark;
  int age;

  Dog() {this.age = 5; this.bark = "woof";}
  Dog(int age) {this.age = age; this.bark = "woof";}
  Dog (int age, String bark) {this.age = age; this.bark = bark;}

  void outputAge() {System.out.println("Age is: " + age);}
  void outputBark() {System.out.println("Bark is: " + bark);}

}
  
