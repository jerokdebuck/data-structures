/*
    Darian Hadjiabadi
    dhadjia1@gmail.com
    dhadjia1
    SparseGraph
*/
import java.util.Iterator;
import java.util.ArrayList;
/**.
    An implementation of Graph<V,E> that
    allows the user to perform multiple
    operations on a graph. This includes
    inserting vertices, inserting nodes,
    removing vertices, removing nodes,
    labeling, and returning iterables
    @param <V> the vertex generic type
    @param <E> the edge generic type
*/
public class SparseGraph<V, E> implements Graph<V, E> {
    private class Vert implements Vertex<V> {
        Graph<V, E> color;
        V data;
        ArrayList<Edge<E>> incomingEdges;
        ArrayList<Edge<E>> outgoingEdges;
        Object label;
        public Vert(Graph<V, E> color) {
            this.color = color;
            this.label = null;
            this.incomingEdges = new ArrayList<Edge<E>>();
            this.outgoingEdges = new ArrayList<Edge<E>>();
        }
        public V get() {
            return this.data;
        }
        public void put(V v) {
            this.data = v;
        }
    }
    private class Edg implements Edge<E> {
        Graph<V, E> color;
        E data;
        Vert from;
        Vert to;
        Object label;
        public Edg(Graph<V, E> color) {
            this.color = color;
            this.label = null;
        }
        public E get() {
            return this.data;
        }
        public void put(E e) {
            this.data = e;
        }
    }
    private ArrayList<Vertex<V>> vertices;
    private ArrayList<Edge<E>> edges;
    /**. Creates SparseGraph instance */
    public SparseGraph() {
        this.edges = new ArrayList<Edge<E>>();
        this.vertices = new ArrayList<Vertex<V>>();
    }
    /**.
        Insert new vertex
        @param v element to insert
        @return vertex position created
    */
    public Vertex<V> insert(V v) {
        Vertex<V> newVertex = this.new Vert(this);
        newVertex.put(v);
        this.vertices.add(newVertex);
        return newVertex;
    }
    /**.
        Insert new edge
        @param from vertex position where edge starts
        @param to vertex position where edge ends
        @param e element to insert
        @return edge position created to hold element
        @throws IllegalArgumentException if vertex
        position are invalid or if this insertion
        would create a duplicate edge
    */
    public Edge<E> insert(Vertex<V> from, Vertex<V> to, E e)
        throws IllegalArgumentException {
        if (from == to) {
            throw new IllegalArgumentException("? Error: cannot selp loop");
        }
        Edge<E> newEdge = this.new Edg(this);
        newEdge.put(e);
        this.connectEdge(from, to, newEdge);
        this.edges.add(newEdge);
        return newEdge;
    }
    private Vert convertV(Vertex<V> v) {
        Vert vertex;
        if (!(v instanceof SparseGraph.Vert) || v == null) {
            throw new IllegalArgumentException("? Error: Vertex is invalid");
        }
        vertex = (Vert) v;
        if (vertex.color != this) {
            throw new IllegalArgumentException("? Error: Vertex is invalid");
        }
        return vertex;
    }
    private Edg convertE(Edge<E> e) {
        Edg edge;
        if (!(e instanceof SparseGraph.Edg) || (e == null)) {
            throw new IllegalArgumentException("? Error: Edge is invalid");
        }
        edge = (Edg) e;
        if (edge.color != this) {
            throw new IllegalArgumentException("? Error: Edge is invalid");
        }
        return edge;
    }
    private void connectEdge(Vertex<V> from, Vertex<V> to, Edge<E> edge) {
        Vert f = this.convertV(from);
        Vert t = this.convertV(to);
        if (f.outgoingEdges.size() <= t.incomingEdges.size()) {
            for (int i = 0; i < f.outgoingEdges.size(); i++) {
                Edg x = this.convertE(f.outgoingEdges.get(i));
                if (x.to == t) {
                    throw new IllegalArgumentException("? Error: Edge already exists for this position");
                }
            }
        } else {
            for (int i = 0; i < t.incomingEdges.size(); i++) {
                Edg x = this.convertE(t.incomingEdges.get(i));
                if (x.from == f) {
                    throw new IllegalArgumentException("? Error: Edge already exists for this position");
                }
            }
        }
        Edg e = this.convertE(edge);
        e.from = f;
        e.to = t;
        f.outgoingEdges.add(e);
        t.incomingEdges.add(e);
    }
    /**.
        Remove a vertex
        @param v vertex of position to remove
        @return element from destroyed vertex position
        @throws IllegalArgumentException if
        vertex position is invalid or if vertex
        still has incident edges
    */
    public V remove(Vertex<V> v) throws
        IllegalArgumentException {
        Vert vertex = this.convertV(v);
        if (vertex.incomingEdges.size() > 0 || vertex.outgoingEdges.size() > 0) {
            throw new IllegalArgumentException("? Error: Vertex still has incident edges");
        }
        V dat = vertex.get();
        vertex.put(null);
        vertex.incomingEdges = null;
        vertex.outgoingEdges = null;
        vertex.label = null;
        vertex.color = null;
        this.vertices.remove(vertex);
        return dat;
    }
    /**.
        Remove an edge
        @param e edge position to remove
        @return element from destroyed edge pos
        @throws IllegalArgumentException if edge
        position is invalid
    */
    public E remove(Edge<E> e) throws
        IllegalArgumentException {
        Edg edge = this.convertE(e);
        E dat = edge.get();
        edge.to.incomingEdges.remove(e);
        edge.from.outgoingEdges.remove(e);
        edge.color = null;
        edge.data = null;
        edge.from = null;
        edge.to = null;
        edge.label = null;
        this.edges.remove(edge);
        return dat;
    }
    private final class EdgIter implements Iterable<Edge<E>> {
        private ArrayList<Edge<E>> list;
        private EdgIter(ArrayList<Edge<E>> l) {
            this.list = l;
        }
        /* Override iterator() such that remove() does not do anything */
        public Iterator<Edge<E>> iterator() {
            return new Iterator<Edge<E>>() {
                private ArrayList<Edge<E>> start = list;
                private int count = 0;
                public boolean hasNext() {
                    return this.count < this.start.size();
                }
                public Edge<E> next() {
                    return this.start.get(count++);
                }
                public void remove() {
                }
            };
        }
    }
    private final class VertIter implements Iterable<Vertex<V>> {
        private ArrayList<Vertex<V>> list;
        private VertIter(ArrayList<Vertex<V>> l) {
            this.list = l;
        }
        /* Override iterator() such that remove() does not do anything */
        public Iterator<Vertex<V>> iterator() {
            return new Iterator<Vertex<V>>() {
                private ArrayList<Vertex<V>> start = list;
                private int count = 0;
                public boolean hasNext() {
                    return this.count < this.start.size();
                }
                public Vertex<V> next() {
                    return this.start.get(count++);
                }
                public void remove() {
                }
            };
        }
    }
    /**.
        Vertices of a path
        @return Iterrable that can be used to
        explore the vertices of the graph; the
        remove() method does not effect graph
    */
    public Iterable<Vertex<V>> vertices() {
        // for the following 4 methods, iterable
        // has iterator from overriden classes
        Iterable<Vertex<V>> itb = this.new VertIter(this.vertices);
        return itb;
    }
    /**.
        Edges of a graph
        @return Iterable that can be used to
        explore the edges of a graph; the remove()
        method does not effect graph
    */
    public Iterable<Edge<E>> edges() {
        Iterable<Edge<E>> itb = this.new EdgIter(this.edges);
        return itb;
    }
    /**.
        Outgoing edges of a vertex
        @param v vertex position to explore
        @return Iterable that can be used
        to explore the outgoing edges of
        the given vertex; the remove() method
        of the iterator does not effect graph
        @throws IllegalArgumentException if
        vertex position is invalid
    */
    public Iterable<Edge<E>> outgoing(Vertex<V> v) throws
        IllegalArgumentException {
        Vert vertex = this.convertV(v);
        Iterable<Edge<E>> itb = this.new EdgIter(vertex.outgoingEdges);
        return itb;
    }
    /**.
        Incoming edges of a vertex
        @param v vertex position to explore
        @return Iterable that can be used to
        explore the incming edges of the given
        vertex; remove() method of iterator
        does not work.
        @throws IllegalArgumentException if
        vertex position is invalid
    */
    public Iterable<Edge<E>> incoming(Vertex<V> v) throws
        IllegalArgumentException {
        Vert vertex = this.convertV(v);
        Iterable<Edge<E>> itb = this.new EdgIter(vertex.incomingEdges);
        return itb;
    }
    /**.
        Start vertex of edge
        @param e Edge position to explore
        @return vertex position edge starts from
        @throws IllegalArgumentException if edge
        position is invalid
    */
    public Vertex<V> from(Edge<E> e) throws
        IllegalArgumentException {
        Edg edge = this.convertE(e);
        return edge.from;
    }
    /**.
        End vertex of edge
        @param e edge position to explore
        @return vertex position edge leads to
        @throws IllegalArgumentException if
        edge position is invalid
    */
    public Vertex<V> to(Edge<E> e) throws
        IllegalArgumentException {
        Edg edge = this.convertE(e);
        return edge.to;
    }
    /**.
        Label vertex with object
        @param v Vertex position to label
        @param l label object
        @throws IllegalArgumentException if
        vertex position is invalid or label
        is null
    */
    public void label(Vertex<V> v, Object l) throws
        IllegalArgumentException {
        if (l == null) {
            throw new IllegalArgumentException("? Error: Label is null)");
        }
        Vert vertex = this.convertV(v);
        vertex.label = l;
    }
    /**.
        Label edge with object
        @param e edge position to label
        @param l label object
        @throws IllegalArgumentException
        if vertex position is invalid or
        label is null
    */
    public void label(Edge<E> e, Object l) throws
        IllegalArgumentException {
        if (l == null) {
            throw new IllegalArgumentException("? Error: Label is null");
        }
        Edg edge = this.convertE(e);
        edge.label = l;
    }
    /**.
        Vertex label
        @param v vertex position to query
        @return label object (or null if none)
        @throws IllegalArgumentException If vertex
        position is invalid
    */
    public Object label(Vertex<V> v) throws
        IllegalArgumentException {
        Vert vertex = this.convertV(v);
        return vertex.label;
    }
    /**.
        Edge label
        @param e edge position to query
        @return label object (or null if none)
        @throws IllegalArgumentException if
        edge position is invalid
    */
    public Object label(Edge<E> e) throws
        IllegalArgumentException {
        Edg edge = this.convertE(e);
        return edge.label;
    }
    /**.
        Clear all labels
    */
    public void clearLabels() {
        for (int i = 0; i < this.vertices.size(); i++) {
            Vert v = this.convertV(this.vertices.get(i));
            v.label = null;
        }
        for (int j = 0; j < this.edges.size(); j++) {
            Edg e = this.convertE(this.edges.get(j));
            e.label = null;
        }
    }
    /**.
         Get string in specific format
         of all edges and vertices in graph
         @return string representing graph
    */
    public String toString() {
        String output = "digraph {\n";
        for (int i = 0; i < this.vertices.size(); i++) {
            V v = this.vertices.get(i).get();
            output = output + "\t" + "\"" + v + "\"" + ";\n";
        }
        for (int j = 0; j < this.edges.size(); j++) {
            E label = this.edges.get(j).get();
            Edg edge = this.convertE(this.edges.get(j));
            V fromData = edge.from.get();
            V toData = edge.to.get();
            output = output + "\t" + "\"" + fromData + "\"" + " -> " + "\"" + toData + "\"" + " [label=" + "\"" + label + "\"" + "];\n";
        }
        return output + "}";
    }
}
