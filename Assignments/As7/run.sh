echo '###########################'
NOW=$(date +"%r")
echo $NOW
DATE=$(date +"%m-%d-%Y")
echo $DATE
echo 'Finding Kevin'
if [[ -e Kevin.java ]]; then
    echo 'Found correct file'
    javac -Xlint:all Kevin.java
    java Kevin action06.txt "Hanks, Tom"
    echo 'Complete'
else
    echo 'Could not find file'
fi
echo '############################'

