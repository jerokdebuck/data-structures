import java.util.Scanner;
import java.util.Iterator;
import java.lang.Iterable;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class TestGraph {
    private interface Fixture<V, E> {
        Graph<V, E> init();
    }
    @DataPoint
    public static final Fixture<String, Integer> GraphImplementation = new Fixture<String, Integer>() {
        public Graph<String, Integer> init() {
            return new SparseGraph<String, Integer>();
        }
    };
    @Theory
    public void EmptyGraphHasEmptyEdgeAndVertexIterables(Fixture<String, Integer> f) {
        int countVertices = 0;
        int countEdges = 0;

        Graph<String, Integer> g = f.init();
        Iterable<Vertex<String>> itb = g.vertices();
        for (Vertex<String> v : itb) {
            countVertices += 1;
        }
        assertEquals(0, countVertices);
        Iterable<Edge<Integer>> itb2 = g.edges();
        for (Edge<Integer> e : itb2) {
            countEdges += 1;
        }
        assertEquals(0, countEdges);
    }
    @Theory
    public void insertingVertexInitializesCorrectly(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v = g.insert("Hello");
        assertEquals("Hello", v.get());
        Vertex<String> v2 = g.insert("Yes");
        assertEquals("Yes", v2.get());
    }
    @Theory
    public void insertingEdgeInitializesCorrectly(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Yes");
        Edge<Integer> e1 = g.insert(v1, v2, 3);
        assertEquals(3, (int) e1.get());
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void SelfLoopingEdgeCrashes(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Edge<Integer> e1 = g.insert(v1, v1, 0);
    }
    
    @Theory @Test(expected=IllegalArgumentException.class)
    public void UnknownVerticesWillCauseEdgeInsertionCrash(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
       
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("Yes");
        Edge<Integer> e1 = g.insert(v1, f1, 4);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void InsertingSameEdgeCrashes(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Bye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Edge<Integer> e2 = g.insert(v1, v2, 6);
    } 
    @Theory
    public void removeVertexReturnsDatainVertex(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Yes");
        Vertex<String> v2 = g.insert("No");
        String str = g.remove(v2);
        assertEquals("No", str);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void removeVertexCrashIfHasIncidentEdges(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Yes");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        g.remove(v1);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void removeVertexCrashIfPosInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("CS");
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("CSFAIL");
        g.remove(f1);
    }
    @Theory
    public void removeEdgeReturnsValueInEdge(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Goodbye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        int i = g.remove(e1);
        assertEquals(i, 5);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void removeEdgeCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("1");
        Vertex<String> f2 = fail.insert("2");
        Edge<Integer> e1 = fail.insert(f1, f2, 5);
        g.remove(e1);
    }
    @Theory
    public void verticesReturnIterableOfAllVertices(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("GoodBye");
        Iterable<Vertex<String>> itb1 = g.vertices();
        int count = 0;
        for (Vertex<String> vert : itb1) {
            count += 1;
        }
        assertEquals(2, count);
        g.remove(v1);
        Iterable<Vertex<String>> itb2 = g.vertices();
        for (Vertex<String> vert : itb2) {
            count -= 1;
        }
        assertEquals(1, count);
    }
    @Theory
    public void edgesReturnsIterableOfAllEdges(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Goodbye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Edge<Integer> e2 = g.insert(v2, v1, 6);
        Iterable<Edge<Integer>> itb1 = g.edges();
        int count = 0;
        for (Edge<Integer> ed : itb1) {
            count += 1;
        }
        assertEquals(2, count);
        g.remove(e1);
        Iterable<Edge<Integer>> itb2 = g.edges();
        for (Edge<Integer> ed : itb2) {
            count -= 1;
        }
        assertEquals(1, count);
    }
    @Theory
    public void iteratorRemoveFunctionDoesNotEffectGraph(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Bye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Iterable<Edge<Integer>> itb1 = g.edges();
        Iterator<Edge<Integer>> iter1 = itb1.iterator();
        iter1.next();
        iter1.remove();
        Iterable<Edge<Integer>> itb2 = g.edges();
        int count = 0;
        for (Edge<Integer> e : itb2) {
            count += 1;
        }
        assertEquals(1, count);

        Iterable<Vertex<String>> itb3 = g.vertices();
        Iterator<Vertex<String>> iter2 = itb3.iterator();
        iter2.next();
        iter2.remove();
        Iterable<Vertex<String>> itb4 = g.vertices();
        int count2 = 0;
        for (Vertex<String> v : itb4) {
            count2 += 1;
        }
        assertEquals(2, count2);
        Iterable<Edge<Integer>> itb5 = g.incoming(v2);
        Iterator<Edge<Integer>> iter3 = itb5.iterator();
        while (iter3.hasNext()) {
            iter3.next();
            iter3.remove();
        }
        Iterable<Edge<Integer>> itb6 = g.incoming(v2);
        int count3 = 0;
        for (Edge<Integer> e : itb6) {
            count3 += 1;
        }
        assertEquals(1, count3);
        Iterable<Edge<Integer>> itb7 = g.outgoing(v1);
        Iterator<Edge<Integer>> iter4 = itb7.iterator();
        while (iter4.hasNext()) {
            iter4.next();
            iter4.remove();
        }
        Iterable<Edge<Integer>> itb8 = g.outgoing(v1);
        int count4 = 0;
        for (Edge<Integer> e : itb8) {
            count4 += 1;
        }
        assertEquals(1, count4);
    }
        
    @Theory
    public void incomingAndOutGoingEmptyIteratorsIfNoIncomingOrOutGoing(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Iterable<Edge<Integer>> itb1 = g.incoming(v1);
        Iterable<Edge<Integer>> itb2 = g.outgoing(v1);
        int count = 0;
        for (Edge<Integer> e : itb1) {
            count += 1;
        }
        assertEquals(0, count);
        for (Edge<Integer> e : itb2) {
            count += 1;
        }
        assertEquals(0, count);
    }
    @Theory
    public void outgoingReturnsOutgoingEdgesOfVertex(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Goodbye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Vertex<String> v3 = g.insert("Ok");
        Edge<Integer> e2 = g.insert(v1, v3, 6);
        Iterable<Edge<Integer>> itb1 = g.outgoing(v1);
        int count = 0;
        for (Edge<Integer> e : itb1) {
            count += 1;
        }
        assertEquals(2, count);
        Iterable<Edge<Integer>> itb2 = g.outgoing(v2);
        for (Edge<Integer> e : itb2) {
            count -= 1;
        }
        assertEquals(2, count);
        Iterable<Edge<Integer>> itb3 = g.outgoing(v3);
        for (Edge<Integer> e : itb3) {
            count -= 1;
        }
        assertEquals(2, count);
        g.remove(e2);
        Iterable<Edge<Integer>> itb4 = g.outgoing(v1);
        for (Edge<Integer> e : itb4) {
            count -= 1;
        }
        assertEquals(1, count);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void outgoingCrashIfVertexInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("hello");
        Vertex<String> f2 = fail.insert("Goodbye");
        Edge<Integer> e1 = fail.insert(f1, f2, 5);
        g.outgoing(f1);
    }
    @Theory
    public void incomingReturnsAllIncomingEdgesOfVertex(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("Bye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Iterable<Edge<Integer>> itb1 = g.incoming(v2);
        Iterable<Edge<Integer>> itb2 = g.incoming(v1);
        int count = 0;
        for (Edge<Integer> e : itb1) {
            count += 1;
        }
        assertEquals(1, count);
        for (Edge<Integer> e : itb2) {
            count += 1;
        }
        assertEquals(1, count);
        g.remove(e1);
        Iterable<Edge<Integer>>  itb3 = g.incoming(v2);
        for (Edge<Integer> e : itb3) {
            count -= 1;
        }
        assertEquals(1, count);
    }    
    @Theory @Test(expected=IllegalArgumentException.class)
    public void incomingCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("Hello");
        Vertex<String> f2 = fail.insert("Goodbye");
        Edge<Integer> e1 = fail.insert(f1, f2, 5);
        g.incoming(f1);
    }
    @Theory
    public void fromReturnsVertexEdgeIsPointTo(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("1");
        Vertex<String> v2 = g.insert("2");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Vertex<String> t1 = g.from(e1);
        assertEquals(v1, t1);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void fromCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("Hello");
        Vertex<String> f2 = fail.insert("Bye");
        Edge<Integer> e1 = fail.insert(f1, f2, 5);
        Vertex<String> t1 = g.from(e1);
    }
    @Theory
    public void toReturnsVertexEdgeIsPointTo(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("!");
        Vertex<String> v2 = g.insert("?");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Vertex<String> v3 = g.to(e1);
        assertEquals(v2, v3);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void toCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("Hello");
        Vertex<String> f2 = fail.insert("Goodbye");
        Edge<Integer> e1 = fail.insert(f1, f2, 5);
        Vertex<String> v = g.to(e1);
    }
    /* This testcombined both label(Vertex<V>, Object l) and 
        label(Vertex<V>). Depending on the sitation, the 
        Object l may in fact be an Edge, Vertex, etc. This
        run just tests a generic object  */
    @Theory
    public void addingVertexLabelAddsAppropriately(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Object test = new Object();
        g.label(v1, test);
        Object o = g.label(v1);
        assertEquals(test, o);
    }
    /* This tests combined both label(Edge<E>, Object l) and label(Edge<E>) */
    @Theory
    public void addingEdgeLabelAddsAppropriately(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Vertex<String> v2 = g.insert("GoodBye");
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Object test = new Object();
        g.label(e1, test);
        Object o = g.label(e1);
        assertEquals(test, o);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void addingVertexLabelCrashIfLabelNull(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Object o = null;
        g.label(v1, o);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void addingVertexLabelCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("Hello");
        Object o = new Object();
        g.label(f1, o);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void addingEdgeLabelCrashIfLabelIsNull(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Edge<Integer> e = g.insert(g.insert("Hello"), g.insert("Bye"), 5);
        Object o = null;
        g.label(e, o);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void addingEdgeLabelCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Edge<Integer> e = fail.insert(fail.insert("Hello"), fail.insert("Bye"), 5);
        Object o = new Object();
        g.label(e, o);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void returningObjectFromVertexCrashIfPositionInvalid(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("hello");
        Object o = new Object();
        g.label(v1, o);
        Graph<String, Integer> fail = new SparseGraph<String, Integer>();
        Vertex<String> f1 = fail.insert("hello");
        g.label(f1);
    }
    @Theory @Test(expected=IllegalArgumentException.class) 
        public void returningObjectFromEdgeCrashIfPositionInvalid(Fixture<String, Integer> f) {
            Graph<String, Integer> g = f.init();
            Edge<Integer> e = g.insert(g.insert("Hello"), g.insert("Bye"), 5);
            Object o = new Object();
            g.label(e, o);
            Graph<String, Integer> fail = new SparseGraph<String, Integer>();
            Edge<Integer> d = fail.insert(fail.insert("Hello"), fail.insert("Bye"), 5);
            g.label(d);
    }
    @Theory
    public void clearLabelsClearsAllLabel(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        Vertex<String> v1 = g.insert("Hello");
        Object o1 = new Object();
        g.label(v1, o1);
        Vertex<String> v2 = g.insert("Bye");
        Object o2 = new Object();
        g.label(v2, o2);
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        Object o3 = new Object();
        g.label(e1, o3);
        g.clearLabels();
        assertEquals(null, g.label(v1));
        assertEquals(null, g.label(v2));
        assertEquals(null, g.label(e1));
    }
    @Theory
    public void toStringPrintsInCorrectFormat(Fixture<String, Integer> f) {
        Graph<String, Integer> g = f.init();
        String empty = g.toString();
        assertEquals("digraph {\n}", empty);
        Vertex<String> v1 = g.insert("Hello");
        String str1 = g.toString();
        assertEquals("digraph {\n\t\"Hello\";\n}", str1);
        Vertex<String> v2 = g.insert("Bye");
        String str2 = g.toString();
        assertEquals("digraph {\n\t\"Hello\";\n\t\"Bye\";\n}", str2);
        Edge<Integer> e1 = g.insert(v1, v2, 5);
        String str3 = g.toString();
        assertEquals("digraph {\n\t\"Hello\";\n\t\"Bye\";\n\t\"Hello\" -> \"Bye\" [label=\"5\"];\n}", str3);
        g.remove(e1);
        g.remove(v1);
        String str4 = g.toString();
        assertEquals("digraph {\n\t\"Bye\";\n}", str4);
    }
      
        
}    
