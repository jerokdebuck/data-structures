import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashTable;

public final class Spell {
    public static void main(String[] args) throws FileNotFoundException {
        // somehow "load" the dictionary file specified in args[0]
        // read up on java.io.File; note that you can wrap a Scanner
        // around a file object
        // consider using a hash map!
        File f = new File(args[0]);
        if (!f.exists()) {
            throw new FileNotFoundException();
        }
        Scanner dictionaryScanner = new Scanner(f);
        if (dictionaryScanner.hasNextLine()) {
            dictionaryStructure = new AvlTreeMap<String, String>();
        }
        while (dictionaryScanner.hasNextLine()) {
            String dictionaryLine = dictionaryScanner.nextLine();
            String[] dictionaryWords = dictionaryLine.split("[\\s[^a-zA-Z]]+");
            for (String w : dictionaryWords) {
            }
        }
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String[] words = s.split("[\\s[^a-zA-Z]]+");
            for (String word: words) {
                // somehow "check" if word is valid (in the dictionary)
                // println each word that's not valid; you should treat
                // each word as lower-case for the comparison, but you
                // should print "bad" words in their original case
                if (dictionaryStructure == null) {
                    System.out.println(word);
                } else {
                    if (!dictionaryStructure.has(word)) {
                        System.out.println(word);
                    }
                }
            }
        }
    }
}
