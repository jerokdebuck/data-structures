import java.util.Iterator;
import java.util.Scanner;
import java.util.ArrayList;

/**
    OpenAddressHashTable is an Implementation of Map that uses
    quadratic probing to avoid collisions.
    @param K, the key element
    @param V, the value element
*/
public class OpenAddressHashTable<K, V> implements Map<K, V> {
    
    public static class Entry<K, V> {
        K key;
        V value;
        public Entry(K k, V v) {
            this.key = k;
            this.value = v;
        }
        public boolean equals(Object that) {
            return (that instanceof Entry) && (this.key.equals(((Entry) that).key));
        }
        public int hashCode() {
            return this.key.hashCode();
        }
    }
    public class HashIterator implements Iterator<K> {
        private int returned = 0;
        private Iterator<Entry<K,V>> iter;
        HashIterator() {
            this.iter = OpenAddressHashTable.this.data.iterator();
        }
        public boolean hasNext() {
            return this.returned < OpenAddressHashTable.this.size;
        }
        public K next() {
            if (hasNext()) {
                this.returned += 1;
                return this.iter.next().key;
            } else {
                return null;
            }
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    private static final int INITIAL_SIZE = 5;
    private int size;
    private ArrayList<Entry<K, V>> data;
    private int currentSize = INITIAL_SIZE;

    public OpenAddressHashTable() {
        this.data = new ArrayList<>();
    }
    private int getNextPrime(int currentSize) {
        int i = currentSize;
        int returnValue = -1;
        for (; i < 2 * currentSize; ++i) {
            if(this.isPrime(i)) {
                returnValue = i;
            }
        }
        return returnValue;
    }
    private boolean isPrime(int n) {
        if (n % 2 == 0) {
            return false;
        }
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }        
    private void grow() {
        int newSize = getNextPrime(this.currentSize);
        ArrayList<Entry<K, V>> bigger = new ArrayList<Entry<K, V>>();
        for (int i = 0; i < this.currentSize; i++) {
            bigger.set(i, this.data.get(i));
        }
        this.data = bigger;
        this.currentSize = newSize;  
    }
    public void insert(K k, V v) throws IllegalArgumentException {
        if ((float) size / this.size >= .5) {
            this.grow();
        }
        if (this.has(k)) {
            throw new IllegalArgumentException();
        }
        Entry<K, V> e = new Entry<K, V>(k, v);
        int slot = this.hash(k);
        this.quadraticInsertion(e, slot, 0);
    }
    private void quadraticInsertion(Entry<K, V> e, int slot, int repeat) {
        int arrayPos = (slot + (repeat * repeat)) % this.currentSize;
        if (this.data.get(arrayPos) == null) {
            this.data.set(arrayPos, e);
            this.size += 1;
        } else if (this.data.get(arrayPos) != null && repeat < this.currentSize - 1) {
            repeat += 1;
            this.quadraticInsertion(e, slot, repeat);
            
        } else {
            this.grow();
            this.quadraticInsertion(e, slot, repeat);
        }
    }
    private int hash(Object o) {
        return this.abs(o.hashCode()) % this.currentSize;
    }
    private int abs(int i) {
        if (i < 0) {
            return -1 * i;
        } else {
            return i;
        }
    }
    private Entry<K, V> findForSure(K k) {
        Entry<K, V> e = this.find(k);
        if (e == null) {
            throw new IllegalArgumentException();
        }
        return e;
    }
    public void remove(K k) throws IllegalArgumentException {
        Entry<K, V> e = this.findForSure(k);
        this.data.remove(e);
        this.size -= 1;
    }
    public void put(K k, V v) throws IllegalArgumentException {
        Entry<K, V> e = this.findForSure(k);
        e.value = v;
    }
    public V get(K k) throws IllegalArgumentException {
        Entry<K, V> e = this.findForSure(k);
        return e.value;
    }
    private Entry<K, V> quadraticFind(K k, int slot, int repeat) {
        int arrayPos = (slot + (repeat * repeat)) % this.currentSize;
        // bug at line 144 ---> this.data.get(arrayPos)
        if (this.data.get(arrayPos) != null && repeat < this.currentSize - 1) {
            if (this.data.get(arrayPos).key == k) {
                return this.data.get(arrayPos);
            }
            repeat += 1;
            this.quadraticFind(k, slot, repeat);
        }
        return null;
    }
    private Entry<K, V> find(K k) {
        int slot = this.hash(k);
        Entry<K, V> e = this.quadraticFind(k, slot, 0);
        return e;
    }
    public boolean has(K k) {
        return this.find(k) != null;
    }
    public int size() {
        return this.size;
    }
    public Iterator<K> iterator() {
        return new HashIterator();
    }
}
