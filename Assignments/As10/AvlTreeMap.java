// Jiarui Wang ; jwang158@jhu.edu
// Darian Hadjiabadi ; dhadjia1@jhu.edu

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * Ordered maps from comparable keys to arbitrary values.
 *
 * A balanced binary search tree implementation. All map operations are O(log n)
 * worst case. Each iterator operates on a copy of the keys, so changing the
 * tree will not change iterations in progress.
 *
 * @param <K> Type for keys.
 * @param <V> Type for values.
 */
public class AvlTreeMap<K extends Comparable<? super K>, V>
        implements OrderedMap<K, V> {

    private class Node {

        Node left, right;
        int height;
        K key;
        V value;

        Node(K k, V v) {
            // left and right default to null
            this.key = k;
            this.value = v;
        }

        public String toString() {
            return "Node<key: " + this.key
                    + "; value: " + this.value
                    + "; height: " + this.height
                    + ">";
        }
    }
    private Node root;
    private int size;
    private StringBuilder stringBuilder;

    /*
     * /!\ DIAGNOSTICS /!\
     * This part of the code is used in updateHeight() to diagnose the
     * maximum height of the tree. Normally commented out.
     */
    //private int maxHeight;
    @Override
    public int size() {
        return this.size;
    }

    // Return node for given key. This one is iterative
    // but the recursive one from lecture would work as
    // well. (It's just that for finding a node there's
    // really no advantage to using recursion.)
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }

    @Override
    public boolean has(K k) {
        return this.find(k) != null;
    }

    // Return node for given key, throw an exception
    // if the key is not in the tree.
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }

    @Override
    public void put(K k, V v) {
        Node n = this.findForSure(k);
        n.value = v;
    }

    @Override
    public V get(K k) {
        Node n = this.findForSure(k);
        return n.value;
    }

    // Insert given key and value into subtree rooted
    // at given node; return changed subtree with new
    // node added.
    private Node insert(Node n, K k, V v) {
        if (n == null) {
            return new Node(k, v);
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.insert(n.left, k, v);
            // Just came from the left, now do:
            if (!this.balanced(n)) {
                // If this node is not balanced:
                n = this.balance(n);
            }
            this.updateHeight(n);
        } else if (cmp > 0) {
            n.right = this.insert(n.right, k, v);
            // Just came from the right, now do:
            if (!this.balanced(n)) {
                // If this node is not balanced:
                n = this.balance(n);
            }
            this.updateHeight(n);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }
        return n;
    }

    /*
     * Balanced node n at which there is a balance violation in one of its
     * children. This method finds the nodes which to balance with and then
     * calls the other balance method to do the actual pointer changing.
     */
    private Node balance(Node n) {
        // Node n2 is the child of n that has the highest height
        Node n2;
        // Node n3 is the child of n2 that has the highest height
        Node n3;
        // First refers to the direction that n took to get to n2
        // Second refers to the direction that n2 took to get to n3
        // true indicates right, false indicates left
        boolean first;
        boolean second;
        if (height(n.right) > height(n.left)) {
            n2 = n.right;
            // True refers to right
            first = true;
        } else {
            n2 = n.left;
            // False refers to left
            first = false;
        }
        if (height(n2.right) > height(n2.left)) {
            n3 = n2.right;
            // True refers to right
            second = true;
        } else {
            n3 = n2.left;
            // False refers to left
            second = false;
        }
        /*
         * /!\ DIAGNOSTICS /!\
         *
         * This code is for printing out the nodes directly related to this
         * tricky balancing act. Very helpful to see what exactly is going
         * on with the code.
         */
        /*
         System.err.println("---");
         System.err.println(n);
         System.err.println("L: "+n.left);
         System.err.println("R: "+n.right);
         System.err.println(n2);
         System.err.println("L: "+n2.left);
         System.err.println("R: "+n2.right);
         System.err.println(n3);
         System.err.println("L: "+n3.left);
         System.err.println("R: "+n3.right);
         System.err.println("*first: "+first);
         System.err.println("*second: "+second);
         */
        return this.balance(n, first, second);
    }

    /*
     * Balances the node n at which there is a balance violation, where d1
     * refers to the direction to the second unbalanced node and d2 refers
     * to the direction from the second to the third unbalanced node. For d1
     * and d2, true refers to RIGHT and false refers to LEFT.
     */
    private Node balance(Node n, boolean d1, boolean d2) {
        // Create Node n2 according to d1
        if (!d1 && !d2) {
            // Left and left requires only one right rotation
            n = this.rightRotate(n);
        } else if (!d1 && d2) {
            // First left and then right
            n.left = this.leftRotate(n.left);
            n = this.rightRotate(n);
        } else if (d1 && !d2) {
            // First right and then left
            n.right = this.rightRotate(n.right);
            n = this.leftRotate(n);
        } else if (d1 && d2) {
            // Right and right requires one left rotation
            // It is better to be explicit here than to simply use an else
            n = this.leftRotate(n);
        }
        return n;
    }

    /*
     * Right tree rotation about Node n.
     */
    private Node rightRotate(Node n) {
        // It is important to note that n is the root pointer which can be
        // set to point to a different Node.
        Node n2 = n.left;
        n.left = n2.right;
        n2.right = n;
        n = n2;
        // Since we just did a right rotation, the right child of the tree
        // must have its height updated, followed by the root.
        this.updateHeight(n.right);
        this.updateHeight(n);
        return n;
    }

    /*
     * Left tree rotation about Node n.
     */
    private Node leftRotate(Node n) {
        // It is important to note that n is the root pointer which can be
        // set to point to a different Node.
        Node n2 = n.right;
        n.right = n2.left;
        n2.left = n;
        n = n2;
        // Since we just did a left rotation, the left child of the tree
        // must have its height updated, followed by the root.
        this.updateHeight(n.left);
        this.updateHeight(n);
        return n;
    }

    /*
     * Helper to determine height. Null is defined as -1.
     * Usage: n.height = Math.max(height(n.left), height(n.right)) + 1
     */
    private int height(Node n) {
        if (n == null) {
            return -1;
        }
        return n.height;
    }

    /*
     * Takes a node n and updates its height. This is a separate function so
     * that diagnostics can be easily added to it if necessary.
     */
    private void updateHeight(Node n) {
        n.height = Math.max(this.height(n.left), this.height(n.right)) + 1;
        /*
         * /!\ DIAGNOSTICS /!\
         * This code was used to display the maximum height of the tree to
         * assist writing the implementation. Prints to STDERR so that the
         * real output could be redirected to /dev/null while the height
         * information would still be displayed.
         */
        /*
         if (n.height > this.maxHeight) {
         this.maxHeight = n.height;
         System.err.println(this.maxHeight);
         }*/
    }

    /*
     * Takes a node n and returns true if it is balanced according to the
     * AVL condition.
     */
    private boolean balanced(Node n) {
        // AVL condition:
        return Math.abs(this.height(n.left) - this.height(n.right)) <= 1;
    }

    @Override
    public void insert(K k, V v) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.insert(this.root, k, v);
        this.size += 1;
    }

    // Return node with maximum key in subtree rooted
    // at given node. (Iterative version because once
    // again recursion has no advantage here.)
    private Node max(Node n) {
        while (n.right != null) {
            n = n.right;
        }
        return n;
    }

    // Remove node with given key from subtree rooted at
    // given node; return changed subtree with given key
    // missing. (Once again doing this recursively makes
    // it easier to add fancy rebalancing code later.)
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }

        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.remove(n.left, k);
            // Just came from the left, now do:
            if (!this.balanced(n)) {
                // If this node is not balanced:
                n = this.balance(n);
            }
            this.updateHeight(n);
        } else if (cmp > 0) {
            n.right = this.remove(n.right, k);
            // Just came from the right, now do:
            if (!this.balanced(n)) {
                // If this node is not balanced:
                n = this.balance(n);
            }
            this.updateHeight(n);
        } else {
            n = this.remove(n);
        }
        return n;
    }

    // Remove given node and return the remaining tree.
    // Easy if the node has 0 or 1 child; if it has two
    // children, find the predecessor, copy its data to
    // the given node (thus removing the key we need to
    // get rid off), then remove the predecessor node.
    private Node remove(Node n) {
        // 0 and 1 child
        // Actual physical removal
        if (n.left == null) {
            return n.right;
        }
        if (n.right == null) {
            return n.left;
        }

        // 2 children
        Node max = this.max(n.left);
        n.key = max.key;
        n.value = max.value;
        n.left = this.remove(n.left, max.key);
        return n;
    }

    @Override
    public void remove(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        this.root = this.remove(this.root, k);
        this.size -= 1;
    }

    // Recursively add keys from subtree rooted at given node
    // into the given list.
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) {
            return;
        }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }

    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }

    // If we don't have a StringBuilder yet, make one;
    // otherwise just reset it back to a clean slate.
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }

    // Recursively append string representations of keys and
    // values from subtree rooted at given node.
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) {
            return;
        }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }

    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");

        this.toStringHelper(this.root, this.stringBuilder);

        int length = this.stringBuilder.length();
        if (length > 1) {
            // If anything was appended at all, get rid of
            // the last ", " the toStringHelper put in.
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");

        return this.stringBuilder.toString();
    }
}
