/**.
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
Uses an insertion sort alogrithm to sort an array
@param <T> element type
*/
public final class InsertionSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {
/**.
Sorts an array in ascending order
@param array the array to be sorted
*/
    public void sort(Array<T> array) {
        for (int i = 1; i < array.length(); i++) {
            //choose a[i]
            //compare it to a[0],a[1],....,a[i-1]
            T value = array.get(i);
            int j = i;
            while (j > 0 && value.compareTo(array.get(j - 1)) < 0) {
                array.set(j, array.get(j - 1));
                j--;
            }
            array.set(j, value);
        }
    }
/**.
Returns a string with name of sort
@return string the name of the sort
*/
    public String name() {
        return "Insertion Sort";
    }
}
