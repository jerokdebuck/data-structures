/*
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
*/

import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class TestStatableArray 
{

 /* Begin Array<T> interface tests. For each test I initialized  a general 
    StatableArray<T> object with reference type StatableArray<T>. To test the Array<T> interface, 
    I then cast the object to reference type Array<T>. */
  @Test
  public void lengthOfNewArrayIsFine() throws InvalidLengthException
  {
    StatableArray<String> primary = new StatableArray<String>(5,"Data Structures");
    Array<String> lengthTest = primary;

    assertEquals(5,lengthTest.length());
  }

  @Test
  public void contentOfNewArrayIsFine() throws InvalidLengthException, InvalidIndexException
  {
      StatableArray<String> primary = new StatableArray<String>(5,"Data Structures");
      Array<String> contentTest = primary;
      for (int i = 0; i < contentTest.length(); i++)
      {
        assertEquals("Data Structures",contentTest.get(i));
      }
  }

  @Test 
  public void setDoesNotChangeLength() throws InvalidLengthException, InvalidIndexException
  {
      StatableArray<String> primary = new StatableArray<String>(5,"Data Structures");
      Array<String> changeTest = primary;
      for (int i = 0; i < changeTest.length(); i++)
      {
        changeTest.set(i,"Nada");
        assertEquals(5,changeTest.length());
        assertEquals("Nada",changeTest.get(i));
      }

  }

  @Test(expected=InvalidLengthException.class)
  public void zeroLengthBoom() throws InvalidLengthException
  {
    StatableArray<String> zeroBoom = new StatableArray<String>(0,"Hello");
  }

  @Test(expected=InvalidLengthException.class)
  public void negativeLengthBoom() throws InvalidLengthException
  {
    StatableArray<String> negBoom = new StatableArray<String>(-1,"Hello");
  }     

  /* End Array<T> interface tests */

  /* Begin Statable interface tests. Tests from here on out will create a primary StatableArray<T> object with reference
     type StatableArray<T>. Two other objects will be created, one with reference type Array<T> to perform array functions
     and second with reference type Statable to test the Statable interface. Both object will point to the original
     StatableArray<T> object. 
     */
  @Test 
  public void initializedArraysStatsAreAllZero() throws InvalidLengthException
  {
    StatableArray<String> primary = new StatableArray<String>(5,"Hello");
    Statable statChecker = primary;
    assertEquals("StatableArray Statistics" + "\n" + "========================" + "\n" + "Total: " + "0" + "\n" +     "get(): " + "0" + "\n" + "set(): " + "0" + "\n" + "length(): " + "0",statChecker.getStatistics());

  }

  @Test
  public void callingArrayMethodsChangesGetStatistics() throws InvalidIndexException, InvalidLengthException
  {
   
    StatableArray<String> primary = new StatableArray<String>(5,"Hello");
    
    Array<String> secondary = primary;
    secondary.get(2);
    
    Statable tertiary = primary;
    assertEquals("StatableArray Statistics" + "\n" + "========================" + "\n" + "Total: " + "1" + "\n" + "get(): " + "1" + "\n" + "set(): " + "0" + "\n" + "length(): " + "0",tertiary.getStatistics());

    secondary.get(1);
    assertEquals("StatableArray Statistics" + "\n" + "========================" + "\n" + "Total: " + "2" + "\n" +     "get(): " + "2" + "\n" + "set(): " + "0" + "\n" + "length(): " + "0",tertiary.getStatistics());
    
    secondary.set(0,"Daryl");
    secondary.length();

    assertEquals("StatableArray Statistics" + "\n" + "========================" + "\n" + "Total: " + "4" + "\n" +     "get(): " + "2" + "\n" + "set(): " + "1" + "\n" + "length(): " + "1",tertiary.getStatistics());


  }

  @Test
  public void usingResetMakesEverythingZero() throws InvalidLengthException, InvalidIndexException
  {
    StatableArray<String> primary = new StatableArray<String>(5,"Hello");
    Array<String> secondary = primary;
    secondary.set(1,"My name is");
    secondary.get(2);
    secondary.length(); 
    secondary.set(3,"Bob");
    
    Statable tertiary = primary;
    tertiary.resetStatistics();
    assertEquals("StatableArray Statistics" + "\n" + "========================" + "\n" + "Total: " + "0" + "\n" +     "get(): " + "0" + "\n" + "set(): " + "0" + "\n" + "length(): " + "0",tertiary.getStatistics());
  }

  /* End Statable interface tests */
}
