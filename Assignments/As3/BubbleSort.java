/**.
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
BubbleSort implementation
@param <T> element type
*/
public final class BubbleSort<T extends Comparable<T>>
    implements SortingAlgorithm<T> {
  /**.
  Sorts an array through BubbleSort
  @param array an array to be sorted
  */
    public void sort(Array<T> array) {
        //swap will determine if the array is sorted or not
        //if false, then no need to continue
        boolean swap = true;
        while (swap) {
            swap = false;
            for (int i = 0; i < array.length() - 1; i++) {
                if (array.get(i).compareTo(array.get(i + 1)) > 0) {
                    //switch a[i] with a[i+1] if
                    // a[i] > a[i+1]
                    T t = array.get(i + 1);
                    array.set(i + 1, array.get(i));
                    array.set(i, t);
                    swap = true;
                }
            }
        }
    }
  /**.
  Returns name of the sort type
  @return a string which contains the name of the sort
  */
    public String name() {
        return "Bubble Sort";
    }
}
