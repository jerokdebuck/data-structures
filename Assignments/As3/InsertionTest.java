
public class InsertionTest
{
  public static void main(String[] args)
  {
    int[] test = {20,50,10,30,53,10,0,56};
    for(int i = 1; i < test.length; i++)
    {
      int temp = test[i];
      int j = i;
      while (j > 0 && test[j-1] > temp)
      {
        test[j] = test[j-1];
        j--;
      }
      test[j] = temp;
    }
    for (int i = 0; i < test.length; i++)
      System.out.println(test[i]);
  }
}
