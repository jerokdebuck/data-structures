/**.
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
StatableArray<T>, an extension of SimpleArray<T> and
implementation of Array<T> and Statable. Can perform
all functions of an array and can give statistical information.
@param <T> element type
*/
public class StatableArray<T> extends SimpleArray<T>
    implements Statable {
        /* private static final variables created
          to shut up checkstyle about 'magic numbers' */
    private static final int LENGTH_ARRAY = 7;
    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int FIVE = 5;
    private static final int SIX = 6;
    private int numGet;
    private int numSet;
    private int numLength;
        /**.
        Creates instance of StatableArray<T>.
        Initializes statistical variables to 0.
        @param length the length of the array
        @param t the value to fill all slots with
        */
    public StatableArray(int length, T t) {
        super(length, t);
        this.numGet = 0;
        this.numSet = 0;
        this.numLength = 0;
    }
        /**.
        Store new element at index
        @param index index between 0 and length -1 where to put the element
        @param t element to put at the given index
        */
    public void set(int index, T t) {
        this.numSet++;
        super.set(index, t);
    }
        /**.
         Return element stored at index
         @param index index between 0 and length -1 where to get from
         @return the element at the given index
        */
    public T get(int index) {
        this.numGet++;
        return super.get(index);
    }
         /**.
          How many slots in array?
          @return the length of the array
         */
    public int length() {
        this.numLength++;
        return super.length();
    }
         /**.
          Sets all statistical related variables to 0
         */
    public void resetStatistics() {
        this.numGet = 0;
        this.numSet = 0;
        this.numLength = 0;
    }
         /**.
           Returns a string which contains information
           related to the arrays statistics.
           string format as given by PHF
           @return string the string which contains the statistical framwork
         */
    public String getStatistics() {
        int total = this.numGet + this.numSet + this.numLength;
        final String[] statStrings = new String[LENGTH_ARRAY];
        statStrings[ZERO] = "StatableArray Statistics";
        statStrings[ONE] = "========================";
        statStrings[TWO] = "Total: ";
        statStrings[THREE] = "get(): ";
        statStrings[FOUR] = "set(): ";
        statStrings[FIVE] = "length(): ";
        statStrings[SIX] = "\n";
        String lineOne = statStrings[ZERO] + statStrings[SIX];
        String lineTwo = statStrings[ONE] + statStrings[SIX];
        String lineThree = statStrings[TWO] + total + statStrings[SIX];
        String lineFour = statStrings[THREE] + this.numGet + statStrings[SIX];
        String lineFive = statStrings[FOUR] + this.numSet + statStrings[SIX];
        String lineSix = statStrings[FIVE] + this.numLength;
        return lineOne + lineTwo + lineThree + lineFour + lineFive + lineSix;
    }
}
