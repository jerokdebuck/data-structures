/*
Darian Hadjiabadi
dhadjia1@gmail.com 
dhadjia1
*/

import org.junit.Test;
import static org.junit.Assert.assertEquals;
 
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
 
import org.junit.runner.RunWith;

@RunWith(Theories.class) 
public class TestList { 

    private interface Fixture<T> { 
        List<T> init();
    }
    
    @DataPoint
    public static final Fixture<Integer> sentinelList = new Fixture<Integer>() {
        public List<Integer> init() {  
        return new SentinelList<Integer>();
        }
    };

    @Theory
    public void initializedSentinelListZeroLength(Fixture<Integer> f) { 
        List<Integer> l = f.init();
        assertEquals(0, l.length());
    }

    @Theory
    public void initializedSentinelListEmpty(Fixture<Integer> f) {
        List<Integer> l = f.init();
        assertEquals(true, l.empty()); 
        String str = l.toString();
        assertEquals("[]", str);
    }

    @Theory
    public void insertFrontInsertsAtFront(Fixture<Integer> f) {
        
        List<Integer> l = f.init();
        assertEquals(0, l.length());
        l.insertFront(1);
        assertEquals(1, l.length());
        l.insertFront(2);
        assertEquals(2, l.length());
        l.insertFront(3);
        assertEquals(3, l.length());
        String str = l.toString();
        assertEquals("[3  2  1]", str);
    }
    @Theory
    public void insertBackInsertsAtBack(Fixture<Integer> f) { 
        
        List<Integer> l = f.init();
        assertEquals(0, l.length());
        l.insertBack(1);
        assertEquals(1, l.length());
        l.insertBack(2);
        assertEquals(2, l.length());
        l.insertBack(3);
        assertEquals(3, l.length());
        String str = l.toString();
        assertEquals("[1  2  3]", str);
    }

    @Theory
    public void insertBeforeInsertsBeforePosition(Fixture<Integer> f) throws InvalidPositionException { 
          List<Integer> l = f.init();   
          assertEquals(0, l.length());
          Position<Integer> x = l.insertBack(1);
          assertEquals(1, l.length());
          Position<Integer> y = l.insertBack(2);
          assertEquals(2, l.length());
          Position<Integer> z = l.insertBack(3);
          assertEquals(3, l.length());
          l.insertBefore(y, 5);
          assertEquals(4, l.length());
          String str = l.toString();
          assertEquals("[1  5  2  3]", str);
    }

    @Theory
    public void insertAfterInsertsAfterPosition(Fixture<Integer> f) throws InvalidPositionException { 
          
          List<Integer> l = f.init();   
          assertEquals(0, l.length());
          Position<Integer> x = l.insertBack(1);
          assertEquals(1, l.length());
          Position<Integer> y = l.insertBack(2);
          assertEquals(2, l.length());
          Position<Integer> z = l.insertBack(3);
          assertEquals(3, l.length());
          l.insertAfter(z, 5);
          assertEquals(4, l.length());
          String str = l.toString();
          assertEquals("[1  2  3  5]", str);
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void insertBeforeCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException { 
          
          List<Integer> l = f.init();   
          assertEquals(0, l.length());
          Position<Integer> x = l.insertBack(1);
          assertEquals(1, l.length());
          Position<Integer> y = l.insertBack(2);
          assertEquals(2, l.length());
          Position<Integer> z = l.insertBack(3);
          assertEquals(3, l.length());           
      
          List<Integer> m = new SentinelList<Integer>();
          Position<Integer> a = m.insertBack(1);

          l.insertBefore(a, 5);
     }
    @Theory @Test(expected=InvalidPositionException.class)
    public void insertAfterCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException {
          
          List<Integer> l = f.init();   
          assertEquals(0, l.length());
          Position<Integer> x = l.insertBack(1);
          assertEquals(1, l.length());
          Position<Integer> y = l.insertBack(2);
          assertEquals(2, l.length());
          Position<Integer> z = l.insertBack(3);
          assertEquals(3, l.length());    
         
          List<Integer> m = new SentinelList<Integer>();
          Position<Integer> a = m.insertBack(1);
          
          l.insertAfter(a, 5);
     }   
          
     @Theory 
     public void removeFrontRemovesFront(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        l.insertBack(1);
        int t = l.removeFront();
        String str = l.toString();
        assertEquals("[]", str);
        assertEquals(1, t);
        l.insertBack(2);
        l.insertBack(3);
        int s = l.removeFront();
        String str2 = l.toString();
        assertEquals("[3]", str2);  
        assertEquals(2, s);
          
     }
  
     @Theory
     public void removeBackRemovesBack(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        l.insertBack(1);
        int t = l.removeBack();
        String str = l.toString();
        assertEquals("[]", str);
        assertEquals(1, t);
        l.insertBack(2);
        l.insertBack(3);
        int s = l.removeBack();
        String str2 = l.toString();
        assertEquals("[2]", str2);
        assertEquals(3, s);
    }
    
    @Theory @Test(expected=EmptyListException.class)
    public void removeFrontEmptyCrash(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        l.insertFront(1);
        l.removeFront();
        assertEquals(0, l.length());
        l.removeFront();
    }
    
    @Theory @Test(expected=EmptyListException.class) 
    public void removeBackEmptyCrash(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        l.insertBack(1);
        l.removeBack();
        assertEquals(0, l.length());
        l.removeBack();
    }

    @Theory
    public void removeAtRemovesAtPositionP(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertBack(3);
        int t = l.removeAt(x);
        String str = l.toString();
        assertEquals(2, t);
        assertEquals("[3]", str);
    }
    @Theory @Test(expected=InvalidPositionException.class) 
    public void removeAtUninitliazedPositionCrash(Fixture <Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertBack(3);
        
        List<Integer> m = new SentinelList<Integer>(); 
        Position<Integer> a = m.insertFront(4);
      
        l.removeAt(a);     
    }

    @Theory 
    public void frontReturnsFrontPosition(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Position<Integer> test = l.front(); 
        assertEquals(test, y);
    }

    @Theory @Test(expected=EmptyListException.class) 
    public void frontCrashesIfListEmpty(Fixture<Integer> f)  throws EmptyListException { 
        List<Integer> l = f.init();
        assertEquals(0, l.length());
        l.front();
    }

    @Theory
    public void backReturnsBackPosition(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Position<Integer> test = l.back();
        assertEquals(test, x);
    }
    @Theory @Test(expected=EmptyListException.class) 
    public void backCrashesIfListEmpty(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        assertEquals(0, l.length());
        l.back();
    }

    @Theory
    public void frontAndBackDoesNotChangeList(Fixture<Integer> f) throws EmptyListException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        l.front();
        String str = l.toString();
        assertEquals("[1  2]", str);
        l.back();
        String str2 = l.toString();
        assertEquals("[1  2]", str2);
    }
     
    @Theory
    public void firstAndLastReturnsBooleanBasedOnPositionPassed(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        boolean one = l.first(x);
        boolean two = l.last(x);
        Position<Integer> y = l.insertFront(1);
        boolean firstTestTrue = l.first(y);
        boolean firstTestFalse = l.first(x);
        boolean lastTestTrue = l.last(x);
        boolean lastTestFalse = l.last(y);
        
        assertEquals(true, one);
        assertEquals(true, two); 
        assertEquals(true, firstTestTrue);
        assertEquals(false, firstTestFalse);
        assertEquals(true, lastTestTrue);
        assertEquals(false, lastTestFalse);
    }

    @Theory @Test(expected=InvalidPositionException.class)
    public void firstCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        
        List<Integer> fail = new SentinelList<Integer>();
        Position<Integer> a = fail.insertFront(3);
        l.first(a);
    }

    @Theory @Test(expected=InvalidPositionException.class)
    public void lastCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
    
        List<Integer> fail = new SentinelList<Integer>();
        Position<Integer> a = fail.insertBack(3);
        l.last(a);
    }
     
    @Theory
    public void nextGivesPositionOfNextNode(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Position<Integer> b = l.next(y);
        assertEquals(x, b);
    }

    @Theory
    public void previousGivesPreviousPosition(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Position<Integer> b = l.previous(x);
        assertEquals(y, b);
    }
    
    @Theory
    public void nextAndPreviousDontChangeList(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Position<Integer> a = l.next(y);
        Position<Integer> b = l.previous(x);
        String str = l.toString();
        assertEquals("[1  2]", str);
    }

    @Theory @Test(expected=InvalidPositionException.class) 
    public void nextCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
      
        List<Integer> fail = new SentinelList<Integer>();
        Position<Integer> a = fail.insertFront(2);
    
        l.next(a);
    }

    @Theory @Test(expected=InvalidPositionException.class) 
    public void previousCrashesIfPositionDoesNotExist(Fixture<Integer> f) throws InvalidPositionException { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(1);
      
        List<Integer> fail = new SentinelList<Integer>();
        Position<Integer> a = fail.insertFront(1);
        
        l.previous(a);
    }
    
    @Theory @Test(expected=InvalidPositionException.class) 
    public void nextFailsIfItIsLastElement(Fixture<Integer> f) throws InvalidPositionException { 
       List<Integer> l = f.init();
       Position<Integer> x = l.insertFront(1);
       Position<Integer> y = l.insertFront(2);

       l.next(x);
    }
       
    @Theory @Test(expected=InvalidPositionException.class) 
    public void previousFailsIfItIsFirstElement(Fixture<Integer> f) throws InvalidPositionException {
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(1);
        Position<Integer> y = l.insertFront(2);
        l.previous(y);
    } 
   
    @Theory
    public void forwardIteratorIsGood(Fixture<Integer> f) { 
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        
        Iterator<Integer> it = l.forward();
        int t = it.get();
        it.next();
        int s = it.get();
        
        assertEquals(1, t);
        assertEquals(2, s);
    }
   @Theory
   public void backwardIteratorIsGood(Fixture<Integer> f) {
        List<Integer> l = f.init();
        Position<Integer> x = l.insertFront(2);
        Position<Integer> y = l.insertFront(1);
        Iterator<Integer> it = l.backward();
        int t = it.get();
        it.next();
        int s = it.get();
        assertEquals(2, t);
        assertEquals(1, s);
   }

}
