/*
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
*/

/**.
    An implementation of List<T>. Creates a doubly
    linked list with arbitary positions Position<T>
    The beginning and end of the list are guarded
    by "sentinal nodes", which hold the value null.
    Upon initialization, these sentinel nodes are
    created. The purpose of this is to increase
    efficiency in methods by not wasting time checking
    for 'null' values.
    @param <T> the generic type
*/


public class SentinelList<T> implements List<T> {
    // static variables to make checkstyle happy
    static final int THREE = 3;
    static final int FOUR = 4;
    private static class Node<T> implements Position<T> {
        Node<T> next;
        Node<T> prev;
        T data;
        List<T> color;
        Node(T t, List<T> color) {
            this.data = t;
            this.color = color;
        }
        public T get() {
            return this.data;
        }
        public void put(T t) {
            this.data = t;
        }
    }
    private static class Iter<T> implements Iterator<T> {
        private Node<T> current;
        private boolean forward;
        public Iter(Node<T> c, boolean f) {
            this.current = c;
            this.forward = f;
        }
        public T get() {
            return this.current.data;
        }

        public boolean valid() {
            return this.current != null;
        }
        public void next() throws InvalidIteratorException {
            this.current = this.forward ? this.current.next : this.current.prev;
            if (!(this.valid())) {
                throw new InvalidIteratorException();
            }
        }
    }
    private Node<T> first;
    private Node<T> last;
    private int length;
    /**.
        Constructor for a SentinelList<T>. Creates
        a SentinelList instance
    */
    public SentinelList() {
        // Create our sentinel nodes
        Node<T> beginSentinel = new Node<T>(null, this);
        /*
          At initialization, this.first become
          the sentinel node at front of list
          and this.last becomes the sentinel
          node at the back of the list. Both
          point to each other
        */
        this.first = beginSentinel;
        Node<T> endSentinel = new Node<T>(null, this);
        this.last = endSentinel;
        this.first.next = this.last;
        this.last.prev = this.first;
        this.first.prev = null;
        this.last.next = null;
    }
    /**.
        Is the list empty?
        @return true if empty false otherwise
    */
    public boolean empty() {
        /* if length is 0, then the only nodes present
         are the sentinel nodes. Hence, the lists
         is empty */
        return this.length == 0;
    }
    /**.
        How many non sentinel nodes are present?
        @return integer, the number of non
        sentinel nodes in the list
    */
    public int length() {
        return this.length;
    }
    /**.
        Insert at front of list.
        @param t element to insert
        @return position created to hold element
    */
    public Position<T> insertFront(T t) {
        /*
          n.next is given the node in this.first.next
          the node that is getting moved down will
          have its prev field point to n. this.first's
          next field will then point to n and n.prev
          will point to this.first.
        */
        Node<T> n = new Node<T>(t, this);
        this.insertions(1, null, null, n);
        this.length += 1;
        return n;
    }
    /**.
        Insert at back of list
        @param t element to insert
        @return position created to hold element
    */
    public Position<T> insertBack(T t) {
        /*
          n.prev is given the node in this.last.prev
          the node that is getting moved up will
          have its next field point to n. this.last's
          prev field will then point to n and n.next
          will point to this.last
        */
        Node<T> n = new Node<T>(t, this);
        this.insertions(2, null, null, n);
        this.length += 1;
        return n;
    }
    // performs insertions
    private void insertions(int location, Node<T> p, Node<T> q, Node<T> n) {
        switch (location) {
            // insertFront
            case 1:
                n.next = this.first.next;
                this.first.next.prev = n;
                this.first.next = n;
                n.prev = this.first;
                break;
            // insertBack
            case 2:
                n.prev = this.last.prev;
                this.last.prev.next = n;
                this.last.prev = n;
                n.next = this.last;
                break;
            // insertBefore
            case THREE:
                n.next = q;
                n.prev = p;
                q.prev = n;
                p.next = n;
                break;
              // insertAfter
            case FOUR:
                n.next = p;
                n.prev = q;
                q.next = n;
                p.prev = n;
                break;
            default:
                break;
        }
    }
    /**.
        Insert before ap osition
        @param pos position to insert before
        @param t element to insert
        @return position created to hold element
        @throws InvalidPositionException if p is
        invalid for this list
   */
    public Position<T> insertBefore(Position<T> pos,
    T t) throws InvalidPositionException {
        Node<T> q = this.convert(pos);
        Node<T> p = q.prev;
        Node<T> n = new Node<T>(t, this);
        this.insertions(THREE, p, q, n);
        this.length += 1;
        return n;
    }
    /**.
        Insert after a position
        @param pos position to insert before
        @param t element to insert
        @return position created to hold element
        @throws InvalidPositionException if p is
        invalid for this list
    */
    public Position<T> insertAfter(Position<T> pos,
    T t) throws InvalidPositionException {
        Node<T> q = this.convert(pos);
        Node<T> p = q.next;
        Node<T> n = new Node<T>(t, this);
        this.insertions(FOUR, p, q, n);
        this.length += 1;
        return n;
    }
    //performs removals
    private void removals(int location, Node<T> n) {
        switch (location) {
            // removeFront
            case 1:
                this.first.next = this.first.next.next;
                this.first.next.prev.prev = null;
                this.first.next.prev.next = null;
                this.first.next.prev = this.first;
                break;
            // removeBack
            case 2:
                this.last.prev = this.last.prev.prev;
                this.last.prev.next.next = null;
                this.last.prev.next.prev = null;
                this.last.prev.next = this.last;
                break;
            // removeAt
            case THREE:
                n.prev.next = n.next;
                n.next.prev = n.prev;
                n.next = null;
                n.prev = null;
                break;
            default:
                break;
        }
    }
    /**.
        Remove node at front of list
        @return t the data inside the removed node
        @throws EmptyListException if list is empty
    */
    public T removeFront() throws EmptyListException {
        if (this.empty()) {
            throw new EmptyListException();
        }
        // get data from node that is being removed
        T t = this.first.next.data;
        this.removals(1, null);
        this.length -= 1;
        return t;
    }
    /**.
        Remove node at back of list
        @return t the data inside the removed node
        @throws EmptyListException if list is empty
    */
    public T removeBack() throws EmptyListException {
        if (this.empty()) {
            throw new EmptyListException();
        }
        // get data from node that is being removed
        T t = this.last.prev.data;
        this.removals(2, null);
        this.length -= 1;
        return t;
    }
    /**.
        Removes node at a given position p
        @param p the position to remove
        @return t the data inside the removed node
        @throws InvalidPositionException if p is
        invalid for this list
    */
    public T removeAt(Position<T> p) throws InvalidPositionException {
        Node<T> n = this.convert(p);
        T t = n.data;
        this.removals(THREE, n);
        this.length -= 1;
        return t;
    }
    /**.
        Returns the position at the front of the list
        @return the position at front of list
        @throws EmptyListException if the list contains
        no non-sentinel nodes
   */
    public Position<T> front() throws EmptyListException {
        if (this.empty()) {
            throw new EmptyListException();
        }
        return this.first.next;
    }
    /**.
        Returns the position at the back of the list
        @return the position at the back of the list
        @throws EmptyListException if the list
        contains no non-sentinel nodes
    */
    public Position<T> back() throws EmptyListException {
        if (this.empty()) {
            throw new EmptyListException();
        }
        return this.last.prev;
    }
    /**.
        Returns true of false if position given is the
        position at the front of the list
        @param p to examine
        @return true if p is first position, false
        otherwise
        @throws InvalidPositionException if p is
        invalid
    */
    public boolean first(Position<T> p) throws InvalidPositionException {
        return this.first.next == this.convert(p);
    }
    /**.
        Returns true of false if position given is the
        position at the back of the list
        @param p to examine
        @return true if p is first position, false
        otherwise
        @throws InvalidPositionException if p is
        invalid
    */
    public boolean last(Position<T> p) throws InvalidPositionException {
        return this.last.prev == this.convert(p);
    }
    /**.
        Next position
        @param p position to examine
        @return position after p
        @throws InvalidPositionException if p is
        invalid or last position
    */
    public Position<T> next(Position<T> p) throws InvalidPositionException {
        Node<T> n = this.convert(p);
        if (n.next == this.last) {
            throw new InvalidPositionException();
        }
        return n.next;
    }
    /**.
        Previous position
        @param p position to examine
        @return position prior to p
        @throws InvalidPositionException if p is
        invalid or first position
    */
    public Position<T> previous(Position<T> p) throws InvalidPositionException {
        Node<T> n = this.convert(p);
        if (n.prev == this.first) {
            throw new InvalidPositionException();
        }
        return n.prev;
    }
    private Node<T> convert(Position<T> p) {
        /* Converts the position given into a
           node if it can be foudn that position
           exists  in the list */
        Node<T> n;
        if (!(p instanceof Node)) {
            throw new InvalidPositionException();
        }
        n = (Node<T>) p;
        if (n.color != this) {
            throw new InvalidPositionException();
        }
        return n;
    }
    /**.
        Create a forward iterator
        @return Iterator at first list element
    */
    public Iterator<T> forward() {
        return new Iter<T>(this.first.next, true);
    }
    /**.
        Create a backward iterator
        @return Iterator at last list element
    */
    public Iterator<T> backward() {
        return new Iter<T>(this.last.prev, false);
    }
    /**.
        Return string containing all elements in the
        list
        @return string containing list elements
    */
    public String toString() {
        Node<T> n = this.first;
        // skip the first sentinel node
        n = n.next;
        String results = "";
        //iterate until sentinel node at end
        for (int i = 0; i < this.length; i++) {
            results = results + "  " + n.data;
            n = n.next;
        }
        if (this.length > 0) {
            String finalResults = results.substring(2, results.length());
            return "[" + finalResults + "]";
        } else {
            // if list is empty, just return []
            return "[]";
        }
    }
}
