/* 
Darian Hadjiabadi
dhadjia1@gmail.com 
600.226 Assignment 1 Question 1
*/


import java.util.*;

public class Unique
{
  public static void main(String[] args)
  {

    int[] arguements = new int[args.length];
    int arrayCount = 0; 

    if (args.length == 0)
    {
      System.out.println("Please input at least one argument into the command line");
      System.exit(1);
    }

/* Tests if command line arguements are integers. If a non integer comes across, program will identify it, ignore it, and move on to the other command line arguements.  If an integer does come accross, the interger is put into the appropriate place */

/*numNull accounts for non integers that appear
isZero accounts for interger value 0 and for 
non integers that appear  */

   int numNull = 0;
   int isZero = 0;

   for (int j = 0; j < args.length; j++)
   {
        boolean isInt = true;
   
  
        for (int arg = 0; arg < args[j].length(); arg++)
        {
          //Tests for negative integer
          if (arg == 0 && args[j].charAt(arg) == '-')
          {
            if(args[j].length() == 1) 
            {
              isInt = false;
              numNull++;
              isZero++;
            }

            else continue; 
          }
          //Tests if arguement has digits or not
          if(Character.digit(args[j].charAt(arg),10) < 0)
          {
            isInt = false;
            numNull++;
            isZero++;
          }
        } 
        if (isInt == true)
        {
          
          arguements[arrayCount] = Integer.parseInt(args[j]);
          if (arguements[arrayCount] == 0)
            isZero++;

          arrayCount++;
        }
    }
    /*Ouputs unique values in the arguement array */

    boolean unique;
    for (int i = 0; i < args.length ; i++)
    {
      unique = true;
      for (int k = 0; k < i; k++)
      {
        if(arguements[k] == arguements[i])
        {
          //The first number in the array case 
          if (i == 0)
            System.out.println(arguements[i]);
          else
          {
            unique = false;    
            break;
          }
        }
      }
      if (unique == true)
      {
        if(arguements[i] != 0 || isZero > numNull)
          System.out.println(arguements[i]);
        else continue; 
      }
    }
  
  }
}

    
