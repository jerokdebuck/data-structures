

DARIAN HADJIABADI
ASSIGNMENT 1 
600.226
dhadjia1@gmail.com


SOME NOTES 

--All necessary java files can be compiled using the Makefile

WRITING WORK

  Question 2
============================
  
  adt UglyVariable
    uses Any
    defines UglyVariable<T: Any>
  
  operations:
    new: ---> UglyVariable<T>
    initialize: T ---> UglyVariable
    get: UglyVariable -/-> T
    set: UglyVariable X T ---> UglyVariable
  
  preconditions
    get(t) = t if function initialized called, or s, where s is not necessarily equal to t, if not initialized
  
  axiom
    get(new()) = s, where s is a random arbitrary value
    get(set(v,t)) = t
    get(initialize(t)) = t
    

With respect to adt Variable, adt UglyVariable does not
have an input for function "new". As a result, the
value inside the UglyVariable will be random, as decidedby 
the computer program. Therefore in order to initialize the variable, 
UglyVariable contains a function "initialize" so that the user can input 
the value he/she chooses. The 'get' function is now partial due to the 
nature of the unitialized variable.    If get(t) is called, then the output 
will either be t if the initialize function had been called prior. Otherwise, 
it will be an arbitrary value s (s may or may not be equal to t). 

==============================================================


Question 3 - Specifications for ResetableCounter
=================================

adt ResetableCounter
	defines ResetableCounter
	uses Integer
	operations
		new: ---> Counter
		new: Integer ---> Counter
		value: Counter ---> Integer
		up: Counter ---> Counter
		down: Counter ---> Counter
		reset: Counter ---> Counter
	
   axioms
		value(new()) = 0
		value(new(t)) = t
		value(up(c)) > value(c), where c > 0
		value(down(c)) < value(c), where c > 0
		value(reset()) = value(new())
====================================
