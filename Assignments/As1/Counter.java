/*
Darian Hadjiabadi
*/
public interface Counter
{
  public int value();
  public void up();
  public void down();
}
