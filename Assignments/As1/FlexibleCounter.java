
/*
Darian Hadjiabadi
dhadjia1@gmail.com  
600.226 Assignment 1 Question 2 FlexibleCounter
*/

public class FlexibleCounter implements ResetableCounter
{
  private int val;
  private int initial;  // used for reset
  private int count; 

//If no arguements when calling class, fix initial to 
// 5 and set counter to 1

/** Constructor for creating a FlexibleCounter
@param val, the value of the counter
@param count, the number with which to increment or decrement the counter
*/

  FlexibleCounter(int val, int count)
  { this.val = val; initial = val; this.count = count;}

/** Creates value method
@return val, the value of the counter
*/
  public int value() {return this.val;}

/** Creates up method
*/
  public void up() { this.val += count;}

/** Creates down method
*/
  public void down() { this.val -= count;}

/* Creates reset method
*/

  public void reset() { this.val = initial;}
  
   
  public static void main(String[] args)
  {
    ResetableCounter flex = new FlexibleCounter(6,-2);
    assert flex.value() == 6; 
    flex.up();
    assert flex.value() == 4;
    flex.reset();
    assert flex.value() == 6;
    flex.down(); 
    assert flex.value() == 8; 
  }

}
