
/* 
Darian Hadjiabadi
*/

public interface ResetableCounter extends Counter
{
  public void reset();
}
