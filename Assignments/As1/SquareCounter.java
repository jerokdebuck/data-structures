/*
Darian Hadjiabadi
dhadjia1@gmail.com  
600.226 Assignment 1 Question 2- SquareCounter
*/


import java.math.*;

public class SquareCounter implements ResetableCounter
{
  private int val;

/** Constuctor for creating a SquareCounter
*/
  SquareCounter() {this.val = 2;}
    
/** Creates value method
@return val, the value of the counter
*/

  public int value() {return this.val;}

/** Creates up method
*/
  
  public void up() {this.val *= 2;}

/** Creates down method
*/

  public void down() {this.val = (int) Math.ceil(Math.sqrt(val));}
  
/** Creates reset method
*/
  public void reset() {this.val = 2;}
   

  public static void main(String[] args)
  {
    ResetableCounter square = new SquareCounter();
    assert square.value() == 2;
    square.down(); 
    assert square.value() == 2;
    square.up();
    assert square.value() == 4;
    square.up(); 
    assert square.value() == 8;
    square.down();
    assert square.value() == 3;
    square.down();
    assert square.value() == 2;
    square.up(); square.reset();
    assert square.value() == 2;
  }
}

