/*
Darian Hadjiabadi
dhadjia1@gmail.com
600.226 Assignment 1 Question 2- BasicCounter
*/


public class BasicCounter implements ResetableCounter
{
  private int val;

/** Create value method
@return val,the value of the counter
*/
 
  public int value()
  {
    return this.val;
  }

 /** creates up method
*/ 
  public void up()
  {
    this.val += 1;
  }
  
/** creates down method
*/
  public void down()
  {
    this.val -= 1;
  }
/** creates reset method
*/
  public void reset()
  {
    this.val = 0;
  }

public static void main(String[] args)
{
  ResetableCounter basic = new BasicCounter();
  assert basic.value() == 0;
  basic.up();
  assert basic.value() == 1;
  basic.up();
  assert basic.value() == 2;
  basic.down();
  assert basic.value() == 1;
  basic.up(); basic.up();
  assert basic.value() == 3;
  basic.reset();
  assert basic.value() == 0;

} 
}
