/**.
    ADT Stack which allows the user to create a Stack obect
    @param <T> element type
*/
public interface Stack<T> {
    /**.
        Pushes an element  onto the top of the stack
        @param t the element type
    */
    void push(T t);
    /**.
        Remove an element frmo the top of the stack
        @throws EmptyStackException if stack is empty
    */
    void pop() throws EmptyStackException;
    /**.
        Returnst he element found at the top of the stack
        @return the element at the top of the stack
        @throws EmptyStackException if stack is empty
    */
    T top() throws EmptyStackException;
    /**.
        Checks to see if stack is empty
        @return true if stack is empty, false otherwise
    */
    boolean empty();
    /**.
        Returns a string containing all elements in the stack
        in order
        @return the string containing all the elements in the
        elements in the stack
    */
    String toString();
}
