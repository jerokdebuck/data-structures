/*
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
CALC
*/
import java.util.Scanner;
/**.
    Calc is an RPN where the user can push and pull
    various integers into a stack. To push an integer,
    simply input the number. Addition (+), subtraction (-),
    multiplication (*) and division (/) can be performed
    if there exists at least two integers in the stack
    at any given time. To output elementsin the stack,
    type in '?'. To pop the top and return that element,
    type in '.'. To quit program, type in '!'
    */
final class Calc {
    // added to shut up checkstyle
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    static final int SEVEN = 7;
    private Calc() {}
    static void performAddition(Stack<Integer> calculator) {
        if (calculator.empty()) {
            System.err.println("? Error: Stack is empty");
        } else {
            int num1 = calculator.top();
            calculator.pop();
            if (calculator.empty()) {
                System.err.println("? Error: Not enough integers in stack.");
                calculator.push(num1);
            } else {
                int num2 = calculator.top();
                calculator.pop();
                int pushBack = num2 + num1;
                calculator.push(pushBack);
            }
        }
    }
    static void performSubtraction(Stack<Integer> calculator) {
        if (calculator.empty()) {
            System.err.println("? Error: Stack is empty    ");
        } else {
            int num1 = calculator.top();
            calculator.pop();
            if (calculator.empty()) {
                System.err.println("? Error: Not enough integers in stack.");
                calculator.push(num1);
            } else {
                int num2 = calculator.top();
                calculator.pop();
                int pushBack = num2 - num1;
                calculator.push(pushBack);
            }
        }
    }
    static void performMultiplication(Stack<Integer> calculator) {
        if (calculator.empty()) {
            System.err.println("? Error: Stack is empty");
        } else {
            int num1 = calculator.top();
            calculator.pop();
            if (calculator.empty()) {
                System.err.println("? Error: Not enough integers in stack.");
                calculator.push(num1);
            } else {
                int num2 = calculator.top();
                calculator.pop();
                int pushBack = num2 * num1;
                calculator.push(pushBack);
            }
        }
    }
    static void popTop(Stack<Integer> calculator) {
        if (calculator.empty()) {
            System.err.println("? Error: Stack is Empty");
        } else {
            int num = calculator.top();
            calculator.pop();
            System.out.println(num);
        }
    }
    static void performDivision(Stack<Integer> calculator) {
        if (calculator.empty()) {
            System.err.println("? Error: Stack is empty");
        } else {
            int num1 = calculator.top();
            if (num1 == 0) {  
                System.err.println("? Error: Division by 0");
                System.exit(1);
            }
            calculator.pop();
            if (calculator.empty()) {
                System.err.println("? Error: Not enough integers in stack");
                calculator.push(num1);
            } else {
                int num2 = calculator.top();
                    calculator.pop();
                    int pushBack = (int) Math.floor(num2 / num1);
                    calculator.push(pushBack);
            }
        }
    }

    static int operationSearch(String input) {
        if (input.compareTo("+") == 0) {
            return 1;
        } else if (input.compareTo("-") == 0) {
            return 2;
        } else if (input.compareTo("*") == 0) {
            return THREE;
        } else if (input.compareTo("/") == 0) {
            return FOUR;
        } else if (input.compareTo("?") == 0) {
            return FIVE;
        } else if (input.compareTo("!") == 0) {
            return SIX;
        } else if (input.compareTo(".") == 0) {
            return SEVEN;
        } else {
            return 0;
        }
    }
    /**.
        main for Calc
        @param args the input arguements from std.in
        @throws EmptyStackException when stack is empty
    */
    public static void main(String[] args) throws EmptyStackException {
        Stack<Integer> calculator = new ArrayStack<Integer>();
        System.out.print("> ");
        Scanner kb = new Scanner(System.in);
        while (kb.hasNext()) {
            String input = kb.nextLine();
            try {
                int value = Integer.parseInt(input);
                calculator.push(value);
            } catch (NumberFormatException e) {
                int operation =  operationSearch(input);
                if (operation == 0) {
                    System.err.println("? Error: Symbol could not be found");
                } else if (operation == 1) {
                    performAddition(calculator);
                } else if (operation == 2) {
                    performSubtraction(calculator);
                } else if (operation == THREE) {
                    performMultiplication(calculator);
                } else if (operation == FOUR) {
                    performDivision(calculator);
                } else if (operation == FIVE) {
                    String result = calculator.toString();
                    System.out.println(result);
                } else if (operation == SIX) {
                    String result = calculator.toString();
                    System.out.println(result);
                    System.exit(1);
                } else if (operation == SEVEN) {
                    popTop(calculator);
                }
            }
            System.out.print("> ");
        }
    }
}
