/*
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
ARRAYDEQUEUE
*/
/**.
    Implementation of interface Dequeue<T>. Allows the
    user to create an ArrayDequeue<T>, a queue such
    that both ends are susestible for both insertion
    and removal. The user can insertion from the back or
    front, remove from the back or front, and do a number
    of other operations
    @param <T> element type
*/
import java.util.*;
public class ArrayDequeue<T> implements Dequeue<T> {

    private T[] rep;
    private int backPosition;
    private int frontPosition;
    private int numTotal;
    /**.
        Creates an ArrayDequeue instance
    */
    public ArrayDequeue() {
        // initialize with size 2
        this.rep = (T[]) new Object[2];
        // set two integers for the circular array,
        // one for front and one for back
        this.frontPosition = 0;
        this.backPosition = this.rep.length - 1;
    }
    private void grow() {
        int total = this.rep.length;
        T[] bigger = (T[]) new Object[this.rep.length * 2];
        /* If frontPosition > backPosition, there is
        chance that front position will fall of array
        index, so use modulus operator to get back to 0
        */
        int start = this.frontPosition;
        for (int i = 0; i < total; i++) {
            if (start > total) {
                start -= total;
            }
            bigger[i] = this.rep[start % total];
            start += 1;
        }
        this.frontPosition = 0;
        this.backPosition = total - 1;
        this.rep = bigger;
    }
    /**.
        Is the dequeue empty?
        @return true if empty, false otherwise
    */
    public boolean empty() {
        return this.numTotal == 0;
    }
    /**.
        Returns the length of the dequeue
        @return int, the length of the dequeue
    */
    public int length() {
        return this.numTotal;
    }
            /**.
                Inserts an element into the back of the queue
                @param t the element to be inserted
    */
    public void insertBack(T t) {
        if (this.numTotal == this.rep.length) {
            this.grow();
        }
        // chacne backPosition falls off indexes,
        // use modulus operator to return to position 0
        this.backPosition = (this.backPosition + 1) % this.rep.length;
        if (this.backPosition > this.rep.length - 1) {
            this.backPosition -= this.rep.length;
        }
        this.rep[this.backPosition] = t;
        this.numTotal++;
    }
    /**.
        Inserts an element into the front of the dequeue
        @param t the element to be inserted
    */
    public void insertFront(T t) {
        if (this.numTotal == this.rep.length) {
            this.grow();
        }
        this.frontPosition = (this.frontPosition - 1) % this.rep.length;
        // fronPosition may fall off array, use
        // modulus operator to get back to end of array
        if (this.frontPosition < 0) {
            this.frontPosition +=  this.rep.length;
        }
        this.rep[this.frontPosition] = t;
        this.numTotal++;
    }
    /**.
        Removes an element from the back of the dequeue
        @throws EmptyQueueException if dequeue was
        previously empty
    */
    public void removeBack() throws EmptyQueueException {
        if (this.empty()) {
            throw new EmptyQueueException();
        }
        // removes back and decrements backposition
        this.backPosition = (this.backPosition - 1) % this.rep.length;
        this.numTotal--;
    }
    /**.
        Removes an element from the front of the dequeue
        @throws EmptyQueueException if dequeue was
        empty
    */
    public void removeFront() throws EmptyQueueException {
        if (this.empty()) {
            throw new EmptyQueueException();
        }
        //removes front and increments frontposition
        this.frontPosition = (this.frontPosition + 1) % this.rep.length;
        this.numTotal--;
    }
    /**.
        Returns the element in the front of the dequeue
        @return  the element at the front of the dequeue
        @throws EmptyQueueException if dequeue was
        empty
    */
    public T front() throws EmptyQueueException {
        if (this.empty()) {
            throw new EmptyQueueException();
        }
        T value = this.rep[this.frontPosition];
        return value;
    }
    /**.
        Returns the element at the back of the dequeue
        @return the element at the back of the dequeue
        @throws EmptyQueueException if dequeue was
        empty
    */
    public T back() throws EmptyQueueException {
        if (this.empty()) {
            throw new EmptyQueueException();
        }
        T value = this.rep[this.backPosition];
        return value;
    }
    /**.
        Returns a string of all elements in the dequeue
        in order of [top,...,bottom]
        @return the string containing all elements of the
        dequeue
    */
    public String toString() {
        String output = "";
        int total = this.numTotal;
        int start = this.frontPosition;
        // almost same as grow except now just index
        // over the number of filled array slots
        for (int i = 0; i < total; i++) {
            if (start > this.rep.length - 1) {
                start -= this.rep.length;
            }
            output = output + ", " + this.rep[start % this.rep.length];
            start++;
        }
        int length = this.numTotal;
        if (length > 0) {
            String fin = output.substring(2, output.length());
            return  "[" + fin + "]";
        } else {
            return "[" + output + "]";
        }
    }

    public static void main(String[] args) { 
        Dequeue<Integer> test = new ArrayDequeue<Integer>();
        for (int i = 0; i < 500; i++) { 
            test.insertBack(i);
        }
        for (int j = 500; j < 1000; j++) { 
            test.insertFront(j);
        }
        String str = test.toString();
        System.out.println(str);
    }
}
