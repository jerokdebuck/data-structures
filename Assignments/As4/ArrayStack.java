/**.
    An implementation of Stack<T> that allows the user
    to implement a stack using arrays. Array stack is
    initially set to size 1 and double in amoritized time
    when full. Stack operates under first in last out
    assumptions
    @param <T> the element type
*/
public class ArrayStack<T> implements Stack<T> {
    private T[] rep;
    private int used;
    /**.
        Create a new ArrayStack<T> instance
    */
    public ArrayStack() {
        this.rep = (T[]) new Object[1];
    }
    private void grow() {
        T[] bigger = (T[]) new Object[this.rep.length * 2];
        for (int i = 0; i < this.rep.length; i++) {
            bigger[i] = this.rep[i];
        }
        this.rep = bigger;
    }
    /**.
        Pushes an element into the top of the stack
        @param t the element type to be placed into the
        stack
    */
    public void push(T t) {
        if (this.used == this.rep.length) {
            this.grow();
        }
        this.rep[this.used] = t;
        this.used += 1;
    }
    /**. Removes an element from the top of the stack
         @throws EmptyStackException if the stack is
         already empty
    */
    public void pop() throws EmptyStackException {
        if (this.empty()) {
            throw new EmptyStackException();
        }
        this.used -= 1;
    }
    /**.
        Allows the user to see what is at the top of
        the stack without removing it
        @return T the element type
        @throws EmptyStackException if the stack is
        already empty
    */
    public T top() throws EmptyStackException {
        if (this.empty()) {
            throw new EmptyStackException();
        }
        return this.rep[this.used - 1];
    }
    /**.
        Is the stack empty?
        @return true if empty, false otherwise
    */
    public boolean empty() {
        return this.used == 0;
    }
    /**.
        Returns a string containing all current elements
        in the stack from top to bottom
        @return the string containing all elements in the
        stack
    */
    public String toString() {
        int length = this.used;
        String output = "";
        for (int i = length - 1; i > -1; i--) {
            if (this.rep[i] != null) {
                output =  output + "  " + this.rep[i];
            }
        }
        if (length > 0) {
            String fin = output.substring(2, output.length());
            return  "[" + fin + "]";
        } else {
            return "[" + output + "]";
        }
    }
}
