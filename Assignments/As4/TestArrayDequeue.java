/*
Darian Hadjiabadi
dhadjia1
dhadjia1@gmail.com  
TESTARRAYDEQUEUE
*/

import org.junit.Test;
import static org.junit.Assert.assertEquals; 

public class TestArrayDequeue { 

    @Test
    public void initializedArrayIsEmpty() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        assertEquals(true, x.empty());
    }
    @Test(expected = EmptyQueueException.class)
    public void removeFrontEmptyDequeueFails() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        x.removeFront();
    }
    @Test(expected = EmptyQueueException.class) 
    public void removeBackEmptyDequeueFails() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        x.removeBack();
    }
    @Test 
    public void addingAndRemovingChangesDequeueLength() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        assertEquals(0, x.length());
        x.insertFront(2);
        assertEquals(false, x.empty());
        assertEquals(1, x.length());
        x.insertBack(4);
        assertEquals(2, x.length());
        x.removeBack();
        assertEquals(1, x.length());
    }
    @Test 
    public void insertBackGivesCorrectOutput() { 
      Dequeue<Integer> x = new ArrayDequeue<Integer>();
      String empty = x.toString();
      assertEquals("[]", empty);    
      x.insertBack(1);
      String one = x.toString();
      assertEquals("[1]", one);
      x.insertBack(2);
      String two = x.toString();
      assertEquals("[1, 2]", two);
      x.insertBack(3);
      String three = x.toString();
      assertEquals("[1, 2, 3]", three);
      x.insertBack(4);
      String four = x.toString();
      assertEquals("[1, 2, 3, 4]",four);
      x.insertBack(5);
      String five = x.toString();
      assertEquals("[1, 2, 3, 4, 5]",five);
    }
    @Test
    public void insertFrontGivesCorrectOutput() {
        Dequeue<Integer> x = new ArrayDequeue<Integer>(    );
        String empty = x.toString();
        assertEquals("[]", empty); 
        x.insertFront(1);
        String one = x.toString();
        assertEquals("[1]", one);
        x.insertFront(2);
        String two = x.toString();
        assertEquals("[2, 1]", two);
        x.insertFront(3);
        String three = x.toString();
        assertEquals("[3, 2, 1]", three);
        x.insertFront(4);
        String four = x.toString();
        assertEquals("[4, 3, 2, 1]",four);
        x.insertFront(5);
        String five = x.toString();
        assertEquals("[5, 4, 3, 2, 1]",five);
    }
    @Test
    public void combinationInsertionFrontBackGivesCorrectOutput() { 
      Dequeue<Integer> x = new ArrayDequeue<Integer>(); 
      x.insertFront(1);
      String one = x.toString();
      assertEquals("[1]", one);
      x.insertBack(2);
      String two = x.toString();
      assertEquals("[1, 2]", two);
      x.insertFront(3);
      String three = x.toString();
      assertEquals("[3, 1, 2]", three);
      x.insertBack(4);
      String four = x.toString();
      assertEquals("[3, 1, 2, 4]", four);
      x.insertFront(5);
      String five = x.toString(); 
      assertEquals("[5, 3, 1, 2, 4]", five);
    }
    @Test
    public void removeFrontOperates() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        x.insertFront(3);
        x.insertFront(4);
        x.removeFront();
        String one = x.toString();
        assertEquals("[3]", one);
        assertEquals(1, x.length());
        x.insertBack(5);
        x.insertBack(6);
        x.removeFront();
        String two = x.toString();
        assertEquals("[5, 6]", two);
        assertEquals(2, x.length());
    }
    @Test 
    public void removeBackOperates() {
         Dequeue<Integer> x = new ArrayDequeue<Integer    >();
         x.insertFront(3);
         x.insertFront(4);
         x.removeBack();
         String one = x.toString();
         assertEquals("[4]", one);  
         assertEquals(1, x.length());
         x.insertBack(5);
         x.insertBack(6);
         x.removeBack();
         String two = x.toString();
         assertEquals("[4, 5]", two);
         assertEquals(2, x.length());
    }
    @Test (expected = EmptyQueueException.class)
    public void seeinFrontOfEmptyThrowsException() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        int t = x.front();
    }
    @Test (expected = EmptyQueueException.class) 
    public void seeingBackofEmptyThrowsException() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        int t = x.back();
    }
    @Test 
    public void seeingBackGivesCorrectOutput() {
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        x.insertBack(1);
        x.insertBack(2);
        x.insertBack(3);
        int test = x.back();
        assertEquals(3, test);
        String str = x.toString();
        assertEquals("[1, 2, 3]", str);
    }
    @Test
    public void seeingFrontGivesCorrectOutput() { 
        Dequeue<Integer> x = new ArrayDequeue<Integer>();
        x.insertFront(1);
        x.insertFront(2);
        x.insertFront(3);
        int test = x.front();
        assertEquals(3, test);
        String str = x.toString();
        assertEquals("[3, 2, 1]", str);
    }
}

