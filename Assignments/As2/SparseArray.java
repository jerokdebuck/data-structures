/*
 Darian Hadjiabadi
dhadjia1
dhadjia1@gmail.com
Assignment 2 question 3
*/


/** Class SparseArray<T> is an implementation of 
interface Array<T>. Unlike SimpleArray<T>, SparseArray<T>,is a linked list which does not create in memory every index when called in the constructor. Instead, nodes are 
created only when the client sets the value for a specific index. This allows memory to be saved and larger lists
to be created. If get is used on a position that does
not have a node, the default value is returned 
@Author Darian Hadjiabadi
@Version 1.0
*/

public class SparseArray<T> implements Array<T>
{

  /** static class Node<T> is a nested class within class SparseArray<T>. Allows creation of a Node to be prepended to the linked list */ 

  private static class Node<T> //Nested class
  {
    Node<T> next;
    T data;
    int position;
  }

  private int length;
  private Node<T> rep;
  private int numNodes; //Stores number ofnodes created
  private T initial;  //Will keep default value

/** Method prepend takes a Node<T> and adds it to the linked list
  @param Node<T> n, a node
*/
  private void prepend(Node<T> n)
  {
    n.next = this.rep;
    this.rep = n;
  }
/** Method find loops through the linked list to find the Node at a specific index
    @return Node<T>, a node
    @param int index, the value with 
    @exception IndexException
  */
  private Node<T> find(int index) throws IndexException
  {
    if (index < 0 || index >= this.length)
      throw new IndexException();

    Node<T>n = rep;
    while (index > 0) 
    {
      n = n.next;
      index--;
    }

    return n;

  }

/** Constructor, creates an instance of SparseArray object  @param int length, the total length of the Array
  @param T initial, the default data for the Array
  @exception LengthException
  */
  public SparseArray(int length, T initial) throws LengthException
  {
    if (length <= 0)
      throw new LengthException();

    this.initial = initial; 
    this.length = length;
    this.numNodes = 0;
    /*For empty list, do not create nodes
     in constructor. Only create in set method */
  }
  /** Method length return the length of the SparseArray
  @return int, the length of the Array
  */

  public int length() 
  {
    return this.length;
  }
 
  /** Method set sets the data field of a certain node at index to T t
  @param int index, the position of the node to store the data in 
  @param T t, the data that will be stored in the node
  @exception IndexException
*/
  public void set(int index, T t) throws IndexException
  {

    if (index < 0 || index >= this.length)
      throw new IndexException();

      /* If no node initialzied prior, initialzize it now */
    if (this.numNodes == 0)
    {
      Node<T> n = new Node<T>();
      n.data = t;
      n.position = index; 
      this.prepend(n);
      this.numNodes++;
    }
    else
    {
   //found used to determine if the position wa exists 
     boolean found = false; 
      for (int nodesClone = 0; nodesClone < this.numNodes; nodesClone++)
      {
        if (this.find(nodesClone).position == index)
        {
          this.find(nodesClone).data = t;
          found = true; 
          break;
        }
      }
    
      if(found == false)
      {
        Node<T> n = new Node<T>();
        n.data = t;
        n.position = index; 
        this.prepend(n);
        this.numNodes++;
      }
    }
   
  }
  /** Method get, return the data found in the node at position index         
  @param int index, the position to find the node in
  @exception IndexException 
  */

  public T get(int index) throws IndexException
  {
  /* If node with position exists, return the data. Else, return the default value originally called in constructor */
    if (index < 0 || index >= this.length)
      throw new IndexException();

    if (this.numNodes == 0)
    {
      return this.initial;
    }
    else
    {
  
      int getFinder = 0;
      while(getFinder < this.numNodes)
      {
        if (this.find(getFinder).position == index)
          return this.find(getFinder).data; 
        else
          getFinder++;
      }
    }
    return this.initial; 
  }

}

