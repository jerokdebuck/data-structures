/*
Darian Hadjiabadi
dhadjia1
dhadjia1@gmail.com
Assignment 2 Question 3
*/



import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
 
import org.junit.runner.RunWith;
 
@RunWith(Theories.class)
public class TestArray {
 
    private interface Fixture<T> {
        Array<T> init(int length, T t) throws LengthException;
    }

    private interface SparseFixture<T>
    {
      Array<T> init(int length, T initial) throws LengthException;
    }
  
    @DataPoint
    public static final Fixture<String> simpleArray = new Fixture<String>() {
        public Array<String> init(int length, String t) throws LengthException {
            return new SimpleArray<String>(length, t);
        }
    };
    @DataPoint
    public static final Fixture<String> listArray = new Fixture<String>() {
        public Array<String> init(int length, String t) throws LengthException {
            return new ListArray<String>(length, t);
        }
    };
 
    @Theory
    public void lengthOfNewArrayIsFine(Fixture<String> f) throws LengthException{
        Array<String> a = f.init(4, "cs226");
        assertEquals(4, a.length());
    }
 
    @Theory
    public void contentOfNewArrayIsFine(Fixture<String> f) throws LengthException, IndexException {
        Array<String> a = f.init(4, "cs226");
        for (int i = 0; i < a.length(); i++) {
            assertEquals("cs226", a.get(i));
        }
    }
 
    @Theory
    public void setDoesNotChangeLength(Fixture<String> f) throws LengthException, IndexException {
        Array<String> a = f.init(4, "cs226");
        for (int i = 0; i < a.length(); i++) {
            a.set(i, "Peter");
            assertEquals(4, a.length());
            assertEquals("Peter", a.get(i));
        }
    }
 
    @Theory @Test(expected=LengthException.class)
    public void zeroArrayLengthBoom(Fixture<String> f) throws LengthException {
    Array<String> a = f.init(0, "cs226");
    }
 
    @Theory @Test(expected=LengthException.class)
    public void negativeArrayLengthBoom(Fixture<String> f) throws LengthException {
    Array<String> a = f.init(-4, "cs226");
    }

/*The previous code was obtained from the Dropbox folder and tests SimpleArray and ListArray. The following code will test SparseArray */

    @DataPoint
    public static final SparseFixture<String> sparseArray = new SparseFixture<String>() 
    {
      public Array<String> init(int length, String initial) throws LengthException 
        {
            return new SparseArray<String>(length, initial);
        }
    };

    @Theory
    public void lengthofNewArrayIsFine(SparseFixture<String> f) throws LengthException
    {
      Array<String> a = f.init(4,"Charles");
      assertEquals(4,a.length());
    }
  
    @Theory
    public void dataFieldOfInitializedArrayReturnsInitial(SparseFixture<String> f) throws LengthException, IndexException
    {
      Array<String> a = f.init(4,"Charles");
      for (int i = 0; i < a.length(); i++)
        assertEquals("Charles",a.get(i));  
    }
    
    @Theory
    public void settingContentofNewArrayIsFine(SparseFixture<String> f) throws IndexException, LengthException
    {
      Array<String> a = f.init(4, "Charles");
      a.set(2,"Hello");
      for (int i = 0; i < a.length(); i++)
      {
        if (i != 2)
          assertEquals("Charles",a.get(i));
        else
          assertEquals("Hello",a.get(i));
      }
    }

    @Theory
    public void setContentDoesNotTheoreticallyChangeLength(SparseFixture<String> f) throws LengthException, IndexException
    {
      Array<String> a = f.init(4,"Charles");
      a.set(1,"Hello");
      a.set(1,"Yes");
      a.set(3,"No");
      a.set(2,"Goodbye");
      for (int i = 0; i < a.length(); i++)
      {
        if (i == 1)
          assertEquals("Yes",a.get(i));
        else if (i == 2)
          assertEquals("Goodbye",a.get(i));
        else if (i == 3)
          assertEquals("No",a.get(i));
        else
          assertEquals("Charles",a.get(i));
        assertEquals(4,a.length());
      }
    }
    @Theory @Test(expected=LengthException.class)
    public void zeroArrayLengthBoom(SparseFixture<String> f) throws LengthException 
    {
      Array<String> a = f.init(0,"Charles");
    }
 
  @Theory @Test(expected=LengthException.class)
    public void negativeArrayLengthBoom(SparseFixture<String> f) throws LengthException 
    {
      Array<String> a = f.init(-4,"Charles");
    }

  @Theory @Test(expected=IndexException.class)
    public void upperIndexExceptionBoom(SparseFixture<String> f) throws IndexException, LengthException
    {
      Array<String> a = f.init(5,"Charles");
      a.get(5);
    }
  
  @Theory @Test(expected=IndexException.class)
  public void lowerIndexExceptionBoom(SparseFixture<String> f) throws IndexException, LengthException
  {
    Array<String> a = f.init(5,"Charles");
    a.get(-1);
  }
     
}
