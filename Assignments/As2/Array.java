
/* 
Darian Hadjiabadi
dhadjia1@gmail.com
*/



/** Interface Array allows user to create different implements using the specification found in Array.adt. 
*/
  
public interface Array<T>
{
  /** Method for returning length of an Array<T>
  @return integer */
  int length();

  /** Method for setting an index of an Array to a generic value T
    @param int index, the index of the array
    @param T t, the generic data to store in the Array at index
    @exception IndexException
  */
  void set(int index, T t) throws IndexException;

  /** Method for getting the value at a specific index of Array
    @param int index, the index with which to search for int he Array
    @exception IndexException
  */

  T get(int index) throws IndexException; 

}
