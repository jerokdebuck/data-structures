/* 
Darian Hadjiabadi
dhadjia1
dhadjia1@gmail.com 
600.226 Assignment 2 Question 1
*/

import java.util.*;

public class Unique
{
  public static void main(String[] args) throws IndexException, LengthException
  {

    int arrayCount = 0; 
    int arraySize = 2;
    Scanner kb = new Scanner(System.in);

    Array<Integer> valueHolder = new SimpleArray<Integer>(arraySize,0); 
    
/*numNull accounts for non integers that appear
isZero accounts for interger value 0 and for 
non integers that appear  */

   int numNull = 0;
   int isZero = 0;

   while(kb.hasNext()) 
   {
        String input = kb.next();
        boolean isInt = true;
   
  
        for (int arg = 0; arg < input.length(); arg++)
        {
          //Tests for negative integer
          if (arg == 0 && input.charAt(arg) == '-')
          {
            if(input.length() == 1) 
            {
              isInt = false;
              numNull++;
              isZero++;
            }

            else continue; 
          }
          //Tests if arguement has digits or not
          if(Character.digit(input.charAt(arg),10) < 0)
          {
            isInt = false;
            numNull++;
            isZero++;
          }
        } 
        if (isInt == true)
        {
          
          valueHolder.set(arrayCount,Integer.parseInt(input));
          if (valueHolder.get(arrayCount) == 0)
            isZero++;

          arrayCount++;
          if (arrayCount == arraySize)
          {
            arraySize *= 2;
            Array<Integer> newHolder = new SimpleArray<Integer>(arraySize,0);
            for (int i = 0; i < valueHolder.length(); i++)
              newHolder.set(i,valueHolder.get(i));
            valueHolder = newHolder; 
          }
        }
    } 
    /*Ouputs unique values in the arguement array */

    boolean unique;
    for (int i = 0; i < valueHolder.length(); i++)
    {
      unique = true;
      for (int k = 0; k < i; k++)
      {
        if(valueHolder.get(k) == valueHolder.get(i))
        {
          //The first number in the array case 
          if (i == 0)
            System.out.println(valueHolder.get(i));
          else
          {
            unique = false;    
            break;
          }
        }
      }
      if (unique == true)
      {
        if(valueHolder.get(i) != 0 || isZero > numNull)
          System.out.println(valueHolder.get(i));
        else continue; 
      }
    }
  
  }
}

    
