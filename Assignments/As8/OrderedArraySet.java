/*
Mark Wang and Darian Hadjiabadi
Data Structures Assignment 8 Problem 3
*/
import java.util.Iterator;
/**An ordered array implementation of set.
   @param <T> element type
*/
public class OrderedArraySet<T extends Comparable<? super T>>
    implements OrderedSet<T> {
    private class Iter implements Iterator<T> {
        private int current;
        public Iter() {
            this.current = 0;
        }
        public T next() {
            T t = OrderedArraySet.this.rep[this.current];
            this.current += 1;
            return t;
        }
        public boolean hasNext() {
            return this.current < OrderedArraySet.this.used;
        }
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
    private T[] rep;
    private int used;

    /**Intializes the ordered array data structure with used = 0
       and array as a comparable array of size 1.
    */
    public OrderedArraySet() {
        this.rep = (T[]) new Comparable<?>[1];
        this.used = 0;
    }

    /**find the location of the object if found or the size of the array
       if not found.
       @param t the object one wants to find
       @return interger of the location of the object
    */
    private int find(T t) {
        int min = 0;
        int max = this.used - 1;
        final double half = 0.5;
        while (max >= min) {
            int mid = (int) (min + (max - min) * half);
            if (this.rep[mid].compareTo(t) < 0) {
                min = mid + 1;
            } else if (this.rep[mid].compareTo(t) > 0) {
                max = mid - 1;
            } else {
                return mid;
            }
        }
        return min;
    }

    /**checks if the integer parameter given contents the object t in
       the T parameter.
       @param t the object one wants to check
       @param i the supposed location of the object
       @return true or false for the answer
    */
    private boolean found(int i, T t) {
        return 0 <= i && i < this.used && this.rep[i].equals(t);
    }

    /**Checks if this object is present in the ordered array.
       @param t the object checked
       @return boolean true for found and false otherwise
    */
    public boolean has(T t) {
        int i = this.find(t);
        return this.found(i, t);
    }

    /**Increase the size of the Array by double the array size.
    */
    private void grow() {
        T[] big = (T[]) new Comparable<?>[2 * this.rep.length];
        for (int i = 0; i < this.used; i++) {
            big[i] = this.rep[i];
        }
        this.rep = big;
    }

    /**Inserts a data member T into the ordered array set.
       @param t the data member to be inserted
    */
    public void insert(T t) {
        int x = this.find(t);
        if (this.found(x, t)) {
            return;
        }
        if (this.used == this.rep.length) {
            this.grow();
        }
        for (int i = this.used; i > x;  i--) {
            this.rep[i] = this.rep[i - 1];
        }
        this.rep[x] = t;
        this.used += 1;
    }

    /**Remove a data member t from the ordred array set.
       @param t the object with this param will be removed*/
    public void remove(T t) {
        int x = this.find(t);
        if (!this.found(x, t)) {
            return;
        }
        for (int i = x; i < this.used - 1; i++) {
            this.rep[i] = this.rep[i + 1];
        }
        this.used -= 1;
    }

    /**Returns the iterator for this data structure.
       @return iterator of type T
    */
    public Iterator<T> iterator() {
        return new Iter();
    }
}
