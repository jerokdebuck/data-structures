import java.util.Comparator;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.DataPoint;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class TestPriorityQueue {
    private interface FixtureDefault<T extends Comparable<? super T>> {
        PriorityQueue<T> init();
    }
    private interface FixtureChange<T extends Comparable<? super T>> {
        PriorityQueue<T> init();
    }
    @DataPoint
    public static final FixtureDefault<Integer> BinaryHeapPriorityQueue = new FixtureDefault<Integer>() {
        public PriorityQueue<Integer> init() {
            return new BinaryHeapPriorityQueue<Integer>();
        }
    };
    @DataPoint
    public static final FixtureChange<Integer> BinaryHeaphPriorityQueue = new FixtureChange<Integer>() {
        public PriorityQueue<Integer> init() {
            return new BinaryHeapPriorityQueue<Integer>(new MinHeap<Integer>());
        }
    };
    
    @Theory
    public void initializedHeapsAreEmpty(FixtureDefault<Integer> f1, FixtureChange<Integer> f2) {
        PriorityQueue<Integer> g1 = f1.init();
        PriorityQueue<Integer> g2 = f2.init();
        assertEquals(true, g1.empty());
        assertEquals(true, g2.empty());
    }
    @Theory @Test(expected=EmptyQueueException.class)
    public void emptyDefaultHeapCrashIfRemove(FixtureDefault<Integer> f1) throws EmptyQueueException {
        PriorityQueue<Integer> g = f1.init();
        g.remove();
    }
    @Theory @Test(expected=EmptyQueueException.class)
    public void emptyChangedHeapCrashIfRemvoe(FixtureChange<Integer> f2) throws EmptyQueueException {
        PriorityQueue<Integer> g = f2.init();
        g.remove();
    }
    @Theory @Test(expected=EmptyQueueException.class)
    public void gettingTopOfDefaultEmptyHeapCrash(FixtureDefault<Integer> f1) throws EmptyQueueException {
        PriorityQueue<Integer> g = f1.init();
        g.top();
    }
    @Theory @Test(expected=EmptyQueueException.class)
    public void gettingTopOfChangedEmptyHeapCrash(FixtureChange<Integer> f2) throws EmptyQueueException {
        PriorityQueue<Integer> g = f2.init();
        g.top();
    }
    @Theory
    public void insertionWorksWithoutCrashing(FixtureDefault<Integer> f1, FixtureChange<Integer> f2) {
        PriorityQueue<Integer> g1 = f1.init();
        PriorityQueue<Integer> g2 = f2.init();
        g1.insert(0);
        g2.insert(0);
        assertEquals(false, g1.empty());
        assertEquals(false, g2.empty());
        assertEquals(0,(int) g1.top());
        assertEquals(0, (int) g2.top());

        for (int i = 1; i < 100001; i++) {
            g1.insert(i);
            g2.insert(i);
        }
    }
    @Theory
    public void topReturnsTopOfHeapBasedOnOrderingProperty(FixtureDefault<Integer> f1, FixtureChange<Integer> f2) {
        PriorityQueue<Integer> g1 = f1.init();
        PriorityQueue<Integer> g2 = f2.init();
        for (int i = 0; i < 1000; i++) {
            g1.insert(i);
            g2.insert(i);
            assertEquals(i, (int) g1.top());
            assertEquals(0, (int) g2.top());
        }
    }
    @Theory
    public void removeHacksOfTopOfQueueAndSwapsAppropriateSuccessorToTop(FixtureDefault<Integer> f1, FixtureChange<Integer> f2) {
        PriorityQueue<Integer> g1 = f1.init();
        PriorityQueue<Integer> g2 = f2.init();
        g1.insert(9);
        g1.insert(7);
        g1.insert(7);
        g1.insert(2);
        g1.insert(5);
        assertEquals(9, (int) g1.top());
        g1.remove();
        assertEquals(7, (int) g1.top());
        g1.remove();
        assertEquals(7, (int) g1.top());
        g1.remove();
        assertEquals(5, (int) g1.top());
 
        g2.insert(5);
        g2.insert(7);
        g2.insert(2);
        g2.insert(9);
        assertEquals(2, (int) g2.top());
        g2.remove();
        assertEquals(5, (int) g2.top());
        g2.remove();
        assertEquals(7, (int) g2.top());
        g2.remove();
        assertEquals(9, (int) g2.top());

    }
}
