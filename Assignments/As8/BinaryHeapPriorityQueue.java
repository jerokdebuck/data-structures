/*
Darian Hadjiabadi and Mark Wang
Data Structures Assignment 8 Problem 4
*/
import java.util.Comparator;
/**.
    An implementation of PriorityQueue<T> that uses a binary
    heap to store values. Important note to all clients: You
    can choose whether you want a min heap or max heap by throwing
    in a Comarator object in the constructor. For min heap, use
    BinaryHeapPriorityQueue(new MinHeap()) and for max heap use
    BinaryHeapPriorityQueue(new MaxHeap()). Default is max heap
    @param <T> type of element value
*/
public class BinaryHeapPriorityQueue<T extends Comparable<? super T>>
    implements PriorityQueue<T> {
    private static final int INITIAL_SIZE = 4;
    private Comparator<? super T> comp;
    private T[] rep;
    private int used = 1;
    /**.
        Creates instance of BinaryHeapPriorityQueue with default set to Max Heap
    */
    public BinaryHeapPriorityQueue() {
        this.rep = (T[]) new Comparable<?>[INITIAL_SIZE];
        this.comp = new MaxHeap<T>();
    }
    /**.
        Creates instance of BinaryHeapPriorityQueue with client chooseing
        whether to use a max heap or min heap
        @param clientComp the type of comparator to be used
    */
    public BinaryHeapPriorityQueue(Comparator<? super T> clientComp) {
        this.rep = (T[]) new Comparable<?>[INITIAL_SIZE];
        this.comp = clientComp;
    }
    private void grow() {
        T[] big = (T[]) new Comparable<?>[2 * this.rep.length];
        for (int i = 1; i < this.used; i++) {
            big[i] = this.rep[i];
        }
        this.rep = big;
    }
    /**.
        Insert a value. Duplicate values end up in the queue.
        so inserting X three times means it needs to be removed
        three times before it's gone again.
        @param t Value to insert
    */
    public void insert(T t) {
        // ignore this.rep[0] to simplify mathematics
        if (this.rep.length == this.used) {
            this.grow();
        }
        int pos = this.used;
        this.rep[pos] = t;
        this.insertionSwap(pos);
        this.used += 1;
    }
    private void insertionSwap(int position) {
        // idea is to bubble through heap, restoring from the bottom
        int i = position;
        while (i > 1) {
            int j = i / 2;
            if (this.comp.compare(this.rep[i], this.rep[j]) < 0) {
                T temp = this.rep[j];
                this.rep[j] = this.rep[i];
                this.rep[i] = temp;
            }
            i = j;
        }
    }
    private void removalSwap() {
        // restore from the top
        int i = 1;
        while ((i * 2) <= this.used) {
            int child;
            if (i * 2 + 1 >= this.used) {
                child = i * 2;
            } else {
                // if children show equality, use left child
                if (this.comp.compare(this.rep[i * 2],
                    this.rep[i * 2 + 1]) <= 0) {
                    child = i * 2;
                } else {
                    child = i * 2 + 1;
                }
            }
            if (this.comp.compare(this.rep[i], this.rep[child]) > 0) {
                T temp = this.rep[i];
                this.rep[i] = this.rep[child];
                this.rep[child] = temp;
            }
            i = child;
        }
    }
    private void deletion() {
        // set top node to last slot and 'remove'
        this.rep[1] = this.rep[this.used - 1];
        this.used -= 1;
        if (this.used > 0) {
            this.removalSwap();
        }
    }
    /**.
        Remove the top value. The top value is the "largest" value in the
        queue as determined by the comparator for the queue
        @throws EmptyQueueException if queue is empty
    */
    public void remove() throws EmptyQueueException {
        if (empty()) {
            throw new EmptyQueueException("Error: Queue is empty");
        }
        this.deletion();
    }
    /**.
        Return top value. The top value is the "largest" value in the
        queue as determined by the comparator for the queue
        @return Top value in the queue
        @throws EmptyQueueException if queue is empty
    */
    public T top() throws EmptyQueueException {
        if (empty()) {
            throw new EmptyQueueException("Error: Queue is empty");
        }
        return this.rep[1];
    }
    /**.
        Is queue empty?
        @return true if empty false otherwise
    */
    public boolean empty() {
        return this.used == 1;
    }
}
/* Created two classes that extended Comparator outside of
   BinaryHeapPriorityClass so that clients can create their
   own instance of MinHeap/MaxHeap. This allows the user
   to determine for themselves which kind of heap to use */
class MinHeap<T extends Comparable<? super T>> implements Comparator<T> {
    public int compare(T t1, T t2) {
        return t1.compareTo(t2);
    }
}
class MaxHeap<T extends Comparable<? super T>> implements Comparator<T> {
    public int compare(T t1, T t2) {
        return t2.compareTo(t1);
    }
}

