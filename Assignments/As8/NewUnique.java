/*
Darian Hadjiabadi and Mark Wang
Updated Unique to work with BinaryHeapPriorityQueue
*/
import java.util.Scanner;

/**
    Filter unique integers.
*/
public final class Unique {
    private static Set<Integer> data;
    private static OrderedSet<Integer> orderedData;
    private static PriorityQueue<Integer> priorityData;
    private static final double NANOS = 1e9;

    private Unique() { /* hide constructor */ }

    /**
        Main method.
        @param args command line arguments
    */
    public static void main(String[] args) {
        priorityData = new BinaryHeapPriorityQueue
            <Integer>(new MinHeap<Integer>());
        Scanner scanner = new Scanner(System.in);
        long before = System.nanoTime();
        while (scanner.hasNextInt()) {
            int i = scanner.nextInt();
            priorityData.insert(i);
        }
        while (!priorityData.empty()) {
            int x = priorityData.top();
            System.out.println(x);
            priorityData.remove();
            // since duplicates all appear next to each other, iterate through
            // duplicates by removing
            while (!priorityData.empty() && priorityData.top() == x) {
                priorityData.remove();
            }
        }
        long duration = System.nanoTime() - before;
        System.err.println(duration / NANOS);
    }
}
