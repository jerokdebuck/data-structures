#!/bin/sh
# Jiarui Wang ; jwang158@jhu.edu
# Darian Hadjiabadi ; dhadjia1@jhu.edu

echo "[#]=============== Assignment 9 profiler script ==============="
echo "`bash --version`"
echo "[ ] System time: `date`"
echo "[*] Re-compiling Words."
rm ./*.class

# Problem 1
javac -Xlint:none WordsBST.java #Binary Search Tree Map
javac -Xlint:none WordsAVL.java #AVL Tree Map
javac -Xlint:none WordsT.java #Treap Map

echo "[*] Begin Profiling."
echo "# Weak 10k test: cracklib10k, dictionary10k, war"
for words in WordsBST WordsAVL WordsT
do
    for i in 1 2 3 4 5
    do
        echo ">$words$i"
        java $words <cracklib10k.txt >/dev/null
        java $words <dictionary10k.txt >/dev/null
        java $words <war.txt >/dev/null
    done
done

echo "# Full test: cracklib, dictionary, war"
for words in WordsAVL WordsT
do
    for i in 1 2 3 4 5
    do
        echo ">Full$words$i"
        java $words <cracklib.txt >/dev/null
        java $words <dictionary.txt >/dev/null
        java $words <war.txt >/dev/null
    done
done

echo "# Starting timing profiling. for cracklib, dictionary, war:"
for words in WordsAVL WordsT
do
    for data in cracklib.txt dictionary.txt war.txt
    do
        echo ">$words,$data"
        java -agentlib:hprof=cpu=times $words <$data >/dev/null
        mv java.hprof.txt profiled/$words$data
    done
done

for data in cracklib10k.txt dictionary10k.txt war.txt
do
    echo ">WordsBST,$data"
    java -agentlib:hprof=cpu=times WordsBST <$data >/dev/null
    mv java.hprof.txt profiled/WordsBST$data
done

#echo "# Continuing timing profiling. UniqueBHPQ at mixed, random, biased 100k:"
#for data in mixed100k.txt random100k.txt biased100k.txt
#do
#    echo ">UniqueBHPQ,$data"
#    java -agentlib:hprof=cpu=times UniqueBHPQ <$data >/dev/null
#    mv java.hprof.txt profiled/UniqueBHPQ$data
#done

#echo "# Starting sampling profiling. All Uniques but BHPQ: 100k, 5 each:"
#for unique in UniqueLL UniqueAL UniqueA UniqueL UniqueMTF UniqueOA
#do
#    for data in mixed100k.txt random100k.txt biased100k.txt
#    do
#        echo ">$unique,$data"
#        for i in 1 2 3 4 5
#        do
#            java -agentlib:hprof=cpu=samples $unique <$data >/dev/null
#            mv java.hprof.txt profiled/$unique$i$data
#        done
#    done
#done

echo "[ ] System time: `date`"
echo "[*] Done!"
