// Jiarui Wang ; jwang158@jhu.edu
// Darian Hadjiabadi ; dhadjia1@jhu.edu

import java.util.Random;

import java.util.Comparator;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.DataPoint;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class TestOrderedMap {
    private interface Fixture<K extends Comparable<? super K>, V> {
        OrderedMap<K,V> init();
    }
    private interface FixtureAvl<K extends Comparable<? super K>, V> {
        OrderedMap<K,V> init();
    }
    @DataPoint
    public static final Fixture<Integer, Integer> binarySearchTreeMap
        = new Fixture<Integer, Integer>() {
        public OrderedMap<Integer, Integer> init() {
            return new BinarySearchTreeMap<Integer, Integer>();
        }
    };
    @DataPoint
    public static final Fixture<Integer, Integer> avlTreeMap
        = new Fixture<Integer, Integer>() {
        public OrderedMap<Integer, Integer> init() {
            return new AvlTreeMap<Integer, Integer>();
        }
    };
    @DataPoint
    public static final Fixture<Integer, Integer> treapMap
        = new Fixture<Integer, Integer>() {
        public OrderedMap<Integer, Integer> init() {
            return new TreapMap<Integer, Integer>();
        }
    };
    @DataPoint
    public static final FixtureAvl<Integer, Integer> avlTreeMapOnly
        = new FixtureAvl<Integer, Integer>() {
        public OrderedMap<Integer, Integer> init() {
            return new AvlTreeMap<Integer, Integer>();
        }
    };
    @DataPoint
    public static final FixtureAvl<Integer, Integer> TreapMapOnly
        = new FixtureAvl<Integer, Integer>() {
        public OrderedMap<Integer, Integer> init() {
            return new TreapMap<Integer, Integer>();
        }
    };
    @Theory
    public void initializedMapIsEmpty(Fixture<Integer, Integer> f) {
        OrderedMap<Integer, Integer> o = f.init();
        assertEquals(0, o.size());
    }
    @Theory
    public void insertLegalKVPairIncreasesSize(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(-11, 1);
        assertEquals(1, o.size());
        o.insert(-2, 2);
        assertEquals(2, o.size());
        o.insert(-1, 2);
        assertEquals(3, o.size());
        Random r = new Random();
        int test;
        int TESTS = 100000;
        int j = 0;
        for (int i = 0; i < TESTS; i++) {
            test = r.nextInt(TESTS);
            if (!o.has(test)) {
                o.insert(test, 2);
                assertEquals(4 + j, o.size());
                j += 1;
            }
        }
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void insertingDuplicateKeyCrashes(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.insert(1, 2);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void insertingNullKeyCausesACrash(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(null, 3);
    }
    @Theory
    public void sizeMethodWorks (Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        assertEquals(o.size(), 0);
        o.insert(1, 1);
        assertEquals(o.size(), 1);
        o.insert(4, 4);
        assertEquals(o.size(), 2);
        o.remove(1);
        assertEquals(o.size(), 1);
        o.remove(4);
        assertEquals(o.size(), 0);
        o.insert(0, 0);
        assertEquals(o.size(), 1);
        o.insert(-90, -90);
        assertEquals(o.size(), 2);
    }
    @Theory
    public void hasMethodReturnsTrueIfKeyExistsElseFalse
        (Fixture<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        boolean x = o.has(1);
        boolean y = o.has(3);
        assertEquals(true, x);
        assertEquals(false, y);
    }
    @Theory
    public void putMethodAllowsChangingValueIfKeyExsits
        (Fixture<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.put(1, 5);
        assertEquals((int) o.get(1), 5);
        o.insert(2, 2);
        o.put(2, 10);
        assertEquals((int) o.get(2), 10);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void putMethodCrashIfKeyDoesNotExist(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.put(2, 5);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void putMethodCrashIfKeyArgumentNull(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.put(null, 2);
    }
    @Theory
    public void getMethodRetunsValueForGivenKey(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 5);
        Integer x = o.get(1);
        assertEquals(5, (int) x);
    }
    @Theory
    public void insertionOfNullValuesIsOkay(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, null);
        Integer x = o.get(1);
        assertEquals(null, x);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void getMethodCrashIfKeyDoesNotExist(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 5);
        o.insert(2, 10);
        Integer x = o.get(3);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void getMethodCrashIfKeyArgumentNull(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 5);
        o.get(null);
    }
    @Theory
    public void removeMethodDecrementsSizeForLegalKeyArugment
        (Fixture<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(-1, 1);
        o.insert(-2, 4);
        o.insert(-3, 9);
        assertEquals(3, o.size());
        o.remove(-1);
        assertEquals(2, o.size());
        o.remove(-3);
        assertEquals(1, o.size());
        o.remove(-2);
        assertEquals(0, o.size());
        int TESTS = 1000;
        for (int i = 0; i < TESTS; i++) {
            o.insert(i, i);
            o.insert(i + 1, i + 1);
            o.insert(i + 2, i + 2);
            o.remove(i + 1);
            o.remove(i + 2);
            assertEquals(i + 1, o.size());
        }
    }
    @Theory
    public void toStringWorks(Fixture<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.insert(2, 2);
        o.insert(3, 3);
        o.insert(4, 4);
        String test = "{1: 1, 2: 2, 3: 3, 4: 4}";
        assertEquals(o.toString(), test);
        o.insert(0, 0);
        test = "{0: 0, 1: 1, 2: 2, 3: 3, 4: 4}";
        assertEquals(o.toString(), test);
    }
    @Theory
    public void insertForAvl(FixtureAvl<Integer, Integer> f)
        throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(0, 0);
        o.insert(1, 1);
        o.insert(2, 2);
        o.insert(-10, -10);
        o.insert(-12, -12);
        String test = "{-12: -12, -10: -10, 0: 0, 1: 1, 2: 2}";
        assertEquals(o.toString(), test);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void removeMethodCrashIfKeyDoesNotExist
        (Fixture<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.insert(2, 4);
        o.insert(3, 6);
        o.remove(4);
    }
    @Theory @Test(expected=IllegalArgumentException.class)
    public void removeMethodCrashIfKeyArgumentNull
        (Fixture<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        o.insert(1, 1);
        o.insert(2, 4);
        o.insert(3, 6);
        o.remove(null);
    }
    @Theory
    public void insertRemoveSortedInAvl
        (FixtureAvl<Integer, Integer> f) throws IllegalArgumentException {
        OrderedMap<Integer, Integer> o = f.init();
        // Must be at least 200k to make sure bad tree overflows
        int MAX_TESTS = 400000;
        for (int i = 0; i < MAX_TESTS; i++) {
            o.insert(i, i);
        }
        Random r = new Random();
        int test;
        for (int i = 0; i < MAX_TESTS; i++) {
            test = r.nextInt(MAX_TESTS);
            if (o.has(test)) {
                o.remove(test);
            }
        }
        for (int i = -2; i < MAX_TESTS + 2; i++) {
            if (!o.has(i)) {
                o.insert(i, i);
            }
        }
        for (int i = -2; i < MAX_TESTS + 2; i++) {
            if (o.has(i)) {
                o.remove(i);
            }
        }
        assertEquals(o.size(), 0);
        o.insert(0, 10);
        assertEquals((int) o.get(0), 10);
    }
}
