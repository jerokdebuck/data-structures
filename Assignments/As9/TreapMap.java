// Jiarui Wang ; jwang158@jhu.edu
// Darian Hadjiabadi ; dhadjia1@jhu.edu

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Random;

/**.
    TreapMap: Implementation of OrderedMap that uses randomely selected integer
    priority values to balance a binary tree (min heap). Worst case for all
    oprations are O(log(n)).
    @param <K> the key
    @param <V> the value
*/

public class TreapMap<K extends Comparable<? super K>, V>
    implements OrderedMap<K, V> {
    private class Node {
        Node left, right;
        K key;
        V value;
        int priority;
        Node(K k, V v, int p) {
            this.key = k;
            this.value = v;
            this.priority = p;
            this.right = null;
            this.left = null;
        }
        public String toString() {
            return "Node<key : " + this.key
                + "; value: " + this.value
                + ">";
        }
    }
    private Node root;
    private int size;
    private StringBuilder stringBuilder;
    private Random randomGenerator = new Random();

    @Override
    public int size() {
        return this.size;
    }
    @Override
    public void insert(K k, V v) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        /*
          It is estimated that height of tree is log(n) where n is the size,
          select a random number between 2^size + 1.
        */
        int randomInt = this.randomGenerator.nextInt((int)
            ((Math.pow(2, this.size) + 1)));
        this.root = this.insert(this.root, k, v, randomInt);
        this.size += 1;
    }
    private Node insert(Node n, K k, V v, int random) {
        if (n == null) {
            return new Node(k, v, random);
        }
        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.insert(n.left, k, v, random);
            n = this.rotationPriorityLogic(n, false, true, random);
        } else if (cmp > 0) {
            n.right = this.insert(n.right, k, v, random);
            n = this.rotationPriorityLogic(n, true, true, random);
        } else {
            throw new IllegalArgumentException("duplicate key " + k);
        }
        return n;
    }
    // if inserted on left side (d1 = false), perform a right rotation
    // else perform a left rotation
    private Node insertionLogic(Node n, boolean d1, int random) {
        int priorityCompare = random - n.priority;
        if (priorityCompare < 0 && !d1) {
            n = this.rotateRight(n);
        } else if (priorityCompare < 0 && d1) {
            n = this.rotateLeft(n);
        }
        return n;
    }
    // if node has two children, compare priorities and swap accordingly
    // else if only right child, do a left rotation then call on remove again
    // this time remove will take argument n.left as the node in question is now
    // in a new spot. if only left child present, do a right rotation then call
    // on remove again this time with argument n.right for same reason as above
    private Node removalLogic(Node n) {
        if (n.left != null && n.right != null) {
            if (n.left.priority > n.right.priority) {
                n = this.rotateRight(n);
                n.right = this.remove(n.right);
            } else {
                n = this.rotateLeft(n);
                n.left = this.remove(n.left);
            }
        } else if (n.left == null && n.right != null) {
            n = this.rotateLeft(n);
            n.left = this.remove(n.left);
        } else if (n.left != null && n.right == null) {
            n = this.rotateRight(n);
            n.right = this.remove(n.right);
        }
        return n;
    }
    // determines whether to insert or remove
    private Node rotationPriorityLogic(Node n, boolean d1,
        boolean d2, int random) {
        if (d2) {
            n = this.insertionLogic(n, d1, random);
        } else if (!d2) {
            n = this.removalLogic(n);
        }
        return n;
    }
    private Node rotateRight(Node n) {
        Node temp = n.left;
        n.left = temp.right;
        temp.right = n;
        n = temp;
        return n;
    }
    private Node rotateLeft(Node n) {
        Node temp = n.right;
        n.right = temp.left;
        temp.left = n;
        n = temp;
        return n;
    }
    private Node remove(Node n) {
        // leaf or null
        if ((n.left == null && n.right == null) || n == null) {
            return null;
        // 2 children
        } else if (n.left != null && n.right != null) {
            n = this.rotationPriorityLogic(n, false, false, 0);
        // 1 child
        } else if (n.left == null) {
            n = this.rotationPriorityLogic(n, false, false, 0);
        } else if (n.right == null) {
            n = this.rotationPriorityLogic(n, false, false, 0);
        }
        return n;
    }
    private Node remove(Node n, K k) {
        if (n == null) {
            throw new IllegalArgumentException("Cannot find key " + k);
        }
        int cmp = k.compareTo(n.key);
        if (cmp < 0) {
            n.left = this.remove(n.left, k);
        } else if (cmp > 0) {
            n.right = this.remove(n.right, k);
        } else {
            n = this.remove(n);
        }
        return n;
    }
    @Override
    public void remove(K k) throws IllegalArgumentException {
        if (k == null) {
            throw new IllegalArgumentException("Cannot remove null key");
        }
        this.root = this.remove(this.root, k);
        this.size -= 1;
    }
    @Override
    public void put(K k, V v) throws IllegalArgumentException {
        Node n = this.findForSure(k);
        n.value = v;
    }
    // iterative way to find a given key
    private Node find(K k) {
        if (k == null) {
            throw new IllegalArgumentException("cannot handle null key");
        }
        Node n = this.root;
        while (n != null) {
            int cmp = k.compareTo(n.key);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return null;
    }
    private Node findForSure(K k) {
        Node n = this.find(k);
        if (n == null) {
            throw new IllegalArgumentException("cannot find key " + k);
        }
        return n;
    }
    @Override
    public V get(K k) throws IllegalArgumentException {
        Node n = this.findForSure(k);
        return n.value;
    }
    @Override
    public boolean has(K k) {
        return this.find(k) != null;
    }
    private void iteratorHelper(Node n, List<K> keys) {
        if (n == null) { return; }
        this.iteratorHelper(n.left, keys);
        keys.add(n.key);
        this.iteratorHelper(n.right, keys);
    }
    @Override
    public Iterator<K> iterator() {
        List<K> keys = new ArrayList<K>();
        this.iteratorHelper(this.root, keys);
        return keys.iterator();
    }
    private void setupStringBuilder() {
        if (this.stringBuilder == null) {
            this.stringBuilder = new StringBuilder();
        } else {
            this.stringBuilder.setLength(0);
        }
    }
    private void toStringHelper(Node n, StringBuilder s) {
        if (n == null) { return; }
        this.toStringHelper(n.left, s);
        s.append(n.key);
        s.append(": ");
        s.append(n.value);
        s.append(", ");
        this.toStringHelper(n.right, s);
    }
    @Override
    public String toString() {
        this.setupStringBuilder();
        this.stringBuilder.append("{");
        this.toStringHelper(this.root, this.stringBuilder);
        int length = this.stringBuilder.length();
        if (length > 1) {
            this.stringBuilder.setLength(length - 2);
        }
        this.stringBuilder.append("}");
        return this.stringBuilder.toString();
    }
}

