/*
Darian Hadjiabadi
dhadjia1@gmail.com
dhadjia1
*/
import java.util.Scanner;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Iterator;
/**.
    DOS is a simple file simulation where the user
    can represent directories in files in a tree system
*/
final class DOS {
    static final int THREE = 3;
    static final int FOUR = 4;
    static final int FIVE = 5;
    static final int SIX = 6;
    static final int SEVEN = 7;
    static final int EIGHT = 8;
    private DOS() {}
    static int analyzeInput(String first) {
        int strval;
        if (first.compareTo("quit") == 0) {
            strval = -1;
        } else if (first.compareTo("ls") == 0) {
            strval = 1;
        } else if (first.compareTo("cd") == 0) {
            strval = 2;
        } else if (first.compareTo("cd ..") == 0) {
            strval = THREE;
        } else if (first.compareTo("mkdir") == 0) {
            strval = FOUR;
        } else if (first.compareTo("rmdir") == 0) {
            strval = FIVE;
        } else if (first.compareTo("mk") == 0) {
            strval = SIX;
        } else if (first.compareTo("rm") == 0) {
            strval = SEVEN;
        } else if (first.compareTo("pwd") == 0) {
            strval = EIGHT;
        } else {
            strval = 0;
        }
        return strval;
    }
    // An implementation of Interface Entry
    private static class File implements Entry {
        String name;
        public void input(String str) {
            this.name = str;
        }
        public String getName() {
            return this.name;
        }
    }
    // An implementation of Interface Entry
    private static class Directory implements Entry {
        String name;
        public void input(String str) {
            this.name = str;
        }
        public String getName() {
            return this.name;
        }
    }
    // If cd is typed in, go to root node
    static Position<Entry> goHome(Tree<Entry> dosTree) {
        Position<Entry> root = dosTree.root();
        return root;
    }
    // if cd .. is typed in, go to parent node
    static Position<Entry> goBack(Tree<Entry> dosTree,
        Position<Entry> currentPos) {
        // if already at root, return arguement position
        if (dosTree.isRoot(currentPos)) {
            return currentPos;
        } else { // else return root position
            Position<Entry> newPos = dosTree.parent(currentPos);
            return newPos;
        }
    }
    // return true if entry does not exist
    static boolean insertionsTest(Tree<Entry> dosTree,
        Position<Entry> currentPos, String entryStr) {
        Iterable<Position<Entry>> itb = dosTree.children(currentPos);
        Iterator<Position<Entry>> iter = itb.iterator();
        // create an iterable of children positions
        // then iterate though
        while (iter.hasNext()) {
            if (iter.next().get().getName().compareTo(entryStr) == 0) {
                System.err.println("? Error: Name already in use");
                return false;
            }
        }
        return true;
    }
    // return true if entry exists
    static Position<Entry> removalTest(Tree<Entry> dosTree
        , Position<Entry> currentPos, String entryStr) {
        Iterable<Position<Entry>> itb = dosTree.children(currentPos);
        Iterator<Position<Entry>> iter = itb.iterator();          // create an iterable of children positions
        // then iterate through
        while (iter.hasNext()) {
            Position<Entry> curr = iter.next();
            if (curr.get().getName().compareTo(entryStr) == 0) {
                return curr;
            }
        }
        System.err.println("? Error: Name not found, could not delete");
        return null;
    }
    // Makes a directory
    static Position<Entry> makeDirectory(Tree<Entry> dosTree,
        Position<Entry> currentPos, String entryStr, Entry entry) {
        // if children exist, call insertionstest
        // else skip this part
        if (dosTree.hasChildren(currentPos)) {
            if (!(insertionsTest(dosTree, currentPos, entryStr))) {
                return currentPos;
            }
        }
        // create a new directory and insert it
        // into tree
        entry = new Directory();
        entry.input(entryStr);
        dosTree.insertChild(currentPos, entry);
        return currentPos;
    }
    // Removes a directory
    static Position<Entry> removeDirectory(Tree<Entry> dosTree,
        Position<Entry> currentPos, String entryStr, Entry entry) {
        // if children exist, call removalTest
        if (dosTree.hasChildren(currentPos)) {
            Position<Entry> curr = removalTest(dosTree, currentPos, entryStr);
            if (curr == null) {
                return currentPos;
            } //Cannot remove a file
            if (curr.get() instanceof File) {
                System.err.println("? Error: Attemping to remove a file, try 'rm' instead");
                return currentPos;
            }
            if (dosTree.hasChildren(curr)) {
                System.err.println("? Error: Directory is not empty");
                return currentPos;
            }
            dosTree.removeAt(curr);
            return currentPos;
        } else {
            System.err.println("? Error: No files or directories present to remove");
            return currentPos;
        }
    }
    // makes a file
    static Position<Entry> makeFile(Tree<Entry> dosTree,
        Position<Entry> currentPos, String entryStr, Entry entry) {
        if (dosTree.hasChildren(currentPos)) {
            if (!(insertionsTest(dosTree, currentPos, entryStr))) {
                return currentPos;
            }
        }
        entry = new File();
        entry.input(entryStr);
        dosTree.insertChild(currentPos, entry);
        return currentPos;
    }
    // removes a file
    static Position<Entry> removeFile(Tree<Entry> dosTree, Position<Entry>
        currentPos, String entryStr, Entry entry) {
        if (dosTree.hasChildren(currentPos)) {
            Position<Entry> curr = removalTest(dosTree, currentPos, entryStr);
            if (curr == null) {
                return currentPos;
            }
            if (curr.get() instanceof Directory) {
                System.err.println("? Error you attemping to remove a directory, try 'rmdir' instead");
                return currentPos;
            }
            dosTree.removeAt(curr);
            return currentPos;
        } else {
            System.err.println("? Error: No files or directories present to remove");
            return currentPos;
        }
    }
    // prints absolute path from root to current
    // position
    static Position<Entry> printAbsPath(Tree<Entry> dosTree,
        Position<Entry> currentPos) {
        if (dosTree.isRoot(currentPos)) {
            System.out.println("/");
            return currentPos;
        }
        String str = "";
        Position<Entry> functionPos = currentPos;
        while (!(dosTree.isRoot(functionPos))) {
            str = "/" + functionPos.get().getName()  + str;
            functionPos = dosTree.parent(functionPos);
        }
        System.out.println(str);
        return currentPos;
    }
    // Print all files and directories that are the
    // children of the current position
    static void printEntries(Tree<Entry> dosTree, Position<Entry> currentPos) {
        if (!dosTree.hasChildren(currentPos)) {
            return;
        }
        Iterable<Position<Entry>> entries = dosTree.children(currentPos);
        Iterator<Position<Entry>> iter = entries.iterator();
        int size = 2;
        ArrayList<String> list = new ArrayList<String>(size);
        while (iter.hasNext()) {
            Position<Entry> curNode = iter.next();
            if (list.size() == size) {
                size *= 2;
                list.ensureCapacity(size);
            }
            Entry curEntry = curNode.get();
            if (curEntry instanceof File) {
                list.add(curEntry.getName());
            } else if (curEntry instanceof Directory) {
                list.add(curEntry.getName() + "/");
            }
        }
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
    // move into arguement directory
    static Position<Entry> changeDirectory(Tree<Entry> dosTree,
        Position<Entry> currentPos, String entryString) {
        if (!dosTree.hasChildren(currentPos)) {
            System.err.println("? Error: No directories to move into");
            return currentPos;
        }
        Iterable<Position<Entry>> itb = dosTree.children(currentPos);
        Iterator<Position<Entry>> iter = itb.iterator();
        Position<Entry> moveInto = null;
        while (iter.hasNext()) {
            Position<Entry> curr = iter.next();
            if (curr.get().getName().compareTo(entryString) == 0) {
                moveInto = curr;
                break;
            }
        }
        if (moveInto == null) {
            System.err.println("? Error: Cannot find directory by that name");
            return currentPos;
        }
        if (moveInto.get() instanceof File) {
            System.err.println("? Error: Cannot move into a file");
            return currentPos;
        }
        return moveInto;
    }
    static Position<Entry> analyzeOperation(int operation, Position<Entry>
        currentPos, Tree<Entry> dosTree, Entry newEntry, String entryString) {
        if (operation == 0) {
            System.err.println("? Error: Unrecognized operation");
        } else if (operation == 1) {
            printEntries(dosTree, currentPos);
        } else if (operation == 2) {
            if (entryString == null) {
                currentPos = goHome(dosTree);
            } else {
                currentPos = changeDirectory(dosTree, currentPos, entryString);
            }
        } else if (operation == THREE) {
            currentPos = goBack(dosTree, currentPos);
        } else if (operation == FOUR) {
            currentPos = makeDirectory(dosTree, currentPos,
                entryString, newEntry);
        } else if (operation == FIVE) {
            currentPos = removeDirectory(dosTree,
                currentPos, entryString, newEntry);
        } else if (operation == SIX) {
            currentPos = makeFile(dosTree, currentPos,
                entryString, newEntry);
        } else if (operation == SEVEN) {
            currentPos = removeFile(dosTree, currentPos,
                entryString, newEntry);
        } else if (operation == EIGHT) {
            currentPos = printAbsPath(dosTree, currentPos);
        }
        return currentPos;
    }
    /**.
        Main for DOS
        @param args the input arguements from std.in
    */
    public static void main(String[] args) {
        Tree<Entry> dosTree = new TreeImplementation<Entry>();
        Entry root = new Directory();
        root.input("/");
        Position<Entry> currentPos = dosTree.insertRoot(root);
        Scanner kb = new Scanner(System.in);
        System.out.print("> ");
        while (kb.hasNext()) {
            int operation;
            String entryString = null;
            String input = kb.nextLine();
            String[] splitted = input.split(" ");
            if (splitted.length > 1) {
                if (splitted[1].compareTo("..") == 0) {
                    splitted[0] = splitted[0] + " " + splitted[1];
                } else {
                    entryString = splitted[1];
                }
            }
            operation = analyzeInput(splitted[0]);
            Entry newEntry = null;
            if (operation == -1) {
                System.exit(1);
            }
            currentPos = analyzeOperation(operation, currentPos,
                dosTree, newEntry, entryString);
            System.out.print("> ");
        }
    }
}
