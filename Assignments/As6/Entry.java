/**.
    General entry
    Darian Hadjiabadi
    dhadjia1@gmail.com
    dhadjia1
*/
public interface Entry {
    /**.
      Put the string value into object
      @param str the string containing the value
    */
    void input(String str);
    /**.
      Return the name passed into the object
      @return string containing the value
    */
    String getName();
}
