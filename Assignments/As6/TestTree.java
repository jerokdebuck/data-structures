/*
Darian Hadjiabadi
TestTree
dhadjia1@gmail.com
dhadjia1
*/
import java.util.Iterator;
import java.lang.Iterable;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.DataPoint;
import org.junit.experimental.theories.Theories;
import org.junit.runner.RunWith;

@RunWith(Theories.class)
public class TestTree { 
    private interface Fixture<T> {
    Tree<T> init();
    }
    @DataPoint
    public static final Fixture<Integer> TreeImplementation = new Fixture<Integer>() {
        public Tree<Integer> init() {
        return new TreeImplementation<Integer>();
        }
    };
    @Theory
    public void initializedTreeIsEmpty (Fixture<Integer> f) {
        Tree<Integer> l = f.init();
        assertEquals(true, l.empty());
    }
    @Theory
    public void initializedTreeHasSizeZero (Fixture<Integer> f) {
        Tree<Integer> l = f.init();
        assertEquals(0, l.size());
    }
    @Theory
    public void validWorksCorrectly (Fixture<Integer> f) {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> y = fail.insertRoot(1);
        assertEquals(true, l.valid(x));
        assertEquals(false, l.valid(y));
    }
    @Theory
    public void hasParentWorksProperly(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        assertEquals(false, l.hasParent(x));
        Position<Integer> y = l.insertChild(x, 2);
        assertEquals(true, l.hasParent(y));
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void hasParentCrashesIfPositionInvalid(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> y = fail.insertRoot(1);
        Position<Integer> z = fail.insertChild(y, 2);
        l.hasParent(z);
    }
    @Theory
    public void hasChildrenWorksProperly(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        assertEquals(true, l.hasChildren(x));
        assertEquals(false, l.hasChildren(y));
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void hasChildrenCrashesIfPositionInvalid(Fixture<Integer> f) throws InvalidPositionException {
          Tree<Integer> l = f.init();
          Position<Integer> x = l.insertRoot(1);
          Tree<Integer> fail = new TreeImplementation<Integer>();
          Position<Integer> y = fail.insertRoot(1);
          l.hasChildren(y);
     }
    @Theory
    public void isRootWorks(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        assertEquals(true, l.isRoot(x));
        assertEquals(false, l.isRoot(y));
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void isRootCrashIfPositionInvalid(Fixture<Integer> f) throws InvalidPositionException {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> y = fail.insertRoot(1);
        l.isRoot(y);
    }
    @Theory
    public void insertRootWorksCorrectly(Fixture<Integer> f) throws InsertionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(5);
        assertEquals(true, l.isRoot(x));
        String str = l.toString();
        assertEquals("[5]", str);
    }
    @Theory @Test(expected=InsertionException.class)
    public void insertRootCrashIfRootExists(Fixture<Integer> f) throws InsertionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(6);
        Position<Integer> y = l.insertRoot(7);
    }
    @Theory
    public void insertChildWorksCorrectly(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        assertEquals(true, l.hasParent(y));
        assertEquals(true, l.hasChildren(x));
        String str = l.toString();
        assertEquals("[1, 2]", str);
    }    
    @Theory @Test(expected=InvalidPositionException.class)    public void insertChildCrashIfPositionInvalid(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> y = fail.insertRoot(2);
        Position<Integer> z = l.insertChild(y, 3);
    }
    @Theory
    public void removeAtWorksCorrectly(Fixture<Integer> f) throws InvalidPositionException, RemovalException
    {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        String str = l.toString();
        assertEquals("[1, 2]", str);
        int removed = l.removeAt(y);
        assertEquals(2, removed);
        String str2 = l.toString();
        assertEquals("[1]", str2);
    }
    @Theory @Test(expected=RemovalException.class)
    public void removeCrashIfPositionHasChildren(Fixture<Integer> f) throws InvalidPositionException, RemovalException {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        Position<Integer> z = l.insertChild(y, 3);
        int removed = l.removeAt(y);
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void removeCrashIfPositionInvalid(Fixture<Integer> f) throws InvalidPositionException, RemovalException {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        Position<Integer> y = l.insertChild(x, 2);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> z = fail.insertRoot(3);
        Position<Integer> w = fail.insertChild(z, 4);
        Position<Integer> s = l.insertChild(z, 5);
    }
    @Theory
    public void sizeChangesWithInsertionsAndRemovals(Fixture<Integer> f) { 
        Tree<Integer> l = f.init();
        assertEquals(0, l.size());
        Position<Integer> x = l.insertRoot(5);
        assertEquals(1, l.size());
        assertEquals(false, l.empty());
        l.removeAt(x);
        assertEquals(0, l.size());
        assertEquals(true, l.empty());
    }
    @Theory
    public void rootWorksCorrectly(Fixture<Integer> f) throws EmptyTreeException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(1);
        assertEquals(x, l.root());
        Position<Integer> y = l.insertChild(x, 3);
        assertEquals(x, l.root());
     }
     @Theory @Test(expected=EmptyTreeException.class)
     public void rootCrashIfEmpty(Fixture<Integer> f) throws EmptyTreeException { 
        Tree<Integer> l = f.init();
        l.root();
    }
     @Theory
     public void parentWorksCorrectly(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(2);
        Position<Integer> y = l.insertChild(x, 3);
        assertEquals(x, l.parent(y));
        Position<Integer> z = l.insertChild(y, 4);
        assertEquals(y, l.parent(z));
     }
     @Theory @Test(expected=InvalidPositionException.class)
     public void parentCrashIfArguementPositionIsRoot (Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(5);
        l.parent(x);
     }
     @Theory @Test(expected=InvalidPositionException.class)
     public void parentCrashIfInvalidPosition (Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> x = fail.insertRoot(5);
        Position<Integer> y = fail.insertChild(x, 2); 
        l.parent(y);
     }
     @Theory
     public void childrenReturnsGoodIterable(Fixture<Integer> f) throws InvalidPositionException {
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(3);
        Position<Integer> c1 = l.insertChild(x, 5);   
        Position<Integer> c2 = l.insertChild(x, 6);
        Position<Integer> c3 = l.insertChild(x, 7);
        Position<Integer> c4 = l.insertChild(x, 8);
        Iterable<Position<Integer>> itb = l.children(x);
        Iterator<Position<Integer>> itr = itb.iterator();
        assertEquals(c1, itr.next()); 
        assertEquals(c2, itr.next());
        assertEquals(c3, itr.next());
        assertEquals(c4, itr.next());
    }
    @Theory @Test(expected=InvalidPositionException.class)
    public void childrenCrashIfInvalidPosition(Fixture<Integer> f) throws InvalidPositionException { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(3);
        Position<Integer> c1 = l.insertChild(x, 5);
        Position<Integer> c2 = l.insertChild(x, 6);
        Position<Integer> c3 = l.insertChild(x, 7);
        Position<Integer> c4 = l.insertChild(x, 8);
        Tree<Integer> fail = new TreeImplementation<Integer>();
        Position<Integer> fail1 = fail.insertRoot(5);
        Position<Integer> fail2 = fail.insertChild(fail1, 6);
        Iterable<Position<Integer>> itb = l.children(fail1);
    }
        
    @Theory
    public void positionsReturnsAllPositions(Fixture<Integer> f) { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(3);
        Position<Integer> a1 = l.insertChild(x, 4);
        Position<Integer> a2 = l.insertChild(x, 5);
        Position<Integer> b1 = l.insertChild(a1, 6);
        Position<Integer> b2 = l.insertChild(a1, 8);
        Position<Integer> b3 = l.insertChild(a2, 7);
        Iterable<Position<Integer>> itb = l.positions();
        Iterator<Position<Integer>> itr = itb.iterator();
        assertEquals(x, itr.next());
        assertEquals(a1, itr.next());
        assertEquals(b1, itr.next());
        assertEquals(b2, itr.next());
        assertEquals(a2, itr.next());
        assertEquals(b3, itr.next());
    }
    @Theory
    public void iteratorReturnsAllElements(Fixture<Integer> f) { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(100);
        Position<Integer> a1 = l.insertChild(x, 4);
        Position<Integer> a2 = l.insertChild(x, 5);
        Position<Integer> b1 = l.insertChild(a1, 6);
        Position<Integer> b2 = l.insertChild(a1, 8);
        Position<Integer> b3 = l.insertChild(a2, 7);
        Iterator<Integer> itr = l.iterator();
        assertEquals(100, (int) itr.next());
        assertEquals(4, (int) itr.next());
        assertEquals(6, (int) itr.next());
        assertEquals(8, (int) itr.next());
        assertEquals(5, (int) itr.next());
        assertEquals(7, (int) itr.next());
    }
    @Theory
    public void toStringGood(Fixture<Integer> f) { 
        Tree<Integer> l = f.init();
        Position<Integer> x = l.insertRoot(100);
        Position<Integer> a1 = l.insertChild(x, 4);
        Position<Integer> a2 = l.insertChild(x, 5);
        Position<Integer> b1 = l.insertChild(a1, 6);
        Position<Integer> b2 = l.insertChild(a1, 8);
        Position<Integer> b3 = l.insertChild(a2, 7);
        String str = l.toString();
        assertEquals("[100, 4, 6, 8, 5, 7]", str);
    }
}
