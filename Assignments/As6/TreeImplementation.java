/*
    Darian Hadjiabadi
    dhadjia1@gmail.com
    dhadjia1
*/
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
/**.
    An implementation of Tree<T>. Uses an ArrayList
    to represent children
    @param <T> the generic type
*/
public class TreeImplementation<T> implements Tree<T> {
    private static class Node<T> implements Position<T> {
        T data;
        Node<T> parent;
        Tree<T> color;
        ArrayList<Position<T>> children;
        Node(T t, Tree<T> color) {
            this.data = t;
            this.color = color;
        }
        public T get() {
            return this.data;
        }
        public void put(T t) {
            this.data = t;
        }
    }
    private int numChildren;
    private int numNodes;
    private Node<T> root;
    /**.
        Create a TreeImplementation Instance
    */
    public TreeImplementation() {
        this.numNodes = 0;
        this.numChildren = 2;
    }
    private Node<T> convert(Position<T> p) throws InvalidPositionException {
        Node<T> n = (Node<T>) p;
        return n;
    }
    /**.
        No elements?
        @return True if tree is empty, flase otherwise
    */
    public boolean empty() {
        return this.numNodes == 0;
    }
    /**.
        How many elements?
        @return Number of elements in tree
    */
    public int size() {
        return this.numNodes;
    }
    /**.
        Is the given position valid for this tree?
        @param p Position to check
        @return True if p is valid, falsa otherwise
    */
    public boolean valid(Position<T> p) {
        Node<T> n;
        if (!(p instanceof Node)) {
            return false;
        }
        n = (Node<T>) p;
        if (n.color != this) {
            return false;
        }
        return true;
    }
    /**.
        Does given position have a parent
        @param p Position to check
        @return True if p has parent, false otherwise
        @throws InvalidPositionException if p !valid
    */
    public boolean hasParent(Position<T> p) throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        if (n.parent != null) {
            return true;
        }
        return false;
    }
    /**.
        Does position have children
        @param p position to check
        @return True if p has children, false otherwise
        @throws InvalidPositionException if p !valid
    */
    public boolean hasChildren(Position<T> p) throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        if (n.children != null) {
            return true;
        }
        return false;
    }
    /**.
        Is given position the root of the tree?
        @param p Position to check
        @return True if p is root of tree, false otherwise
        @throws InvalidPositionException if p !valid
    */
    public boolean isRoot(Position<T> p) throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        if (n.parent != null) {
            return false;
        }
        return true;
    }
    /**.
        Insert at the root of the tree
        @param t Element to store
        @return position of new root
        @throws InsertionException if root already exists
    */
    public Position<T> insertRoot(T t) throws InsertionException {
        if (this.root != null) {
            throw new InsertionException();
        }
        // initialize a node, set its parent
        // to null, increment numNodes, set root to node
        Node<T> n = new Node<T>(t, this);
        n.parent = null;
        n.data = t;
        n.children = null;
        this.numNodes += 1;
        this.root = n;
        return n;
    }
     /**.
        Insert a new child at a given position
        @param p position to insert a child for
        @param t element to store
        @return position of new child
        @throws InvalidPositionException if p !valid
     */
    public Position<T> insertChild(Position<T> p, T t)
        throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        //get node from position
        // if children field is null, create
        // a new ArrayList
        Node<T> n = this.convert(p);
        if (n.children == null) {
            n.children = new ArrayList<Position<T>>(this.numChildren);
        }
        //grow array if full
        if (this.numChildren == n.children.size()) {
            this.numChildren *= 2;
            n.children.ensureCapacity(this.numChildren);
        }
        // make child node
        //assign its parent to the position given
        // and initialize its data field
        Node<T> child = new Node<T>(t, this);
        child.parent = n;
        child.data = t;
        child.children = null;
        n.children.add(child);
        this.numNodes += 1;
        return child;
    }
     /**.
        Remove a given position
        @param p position to remove
        @return element stored at p
        @throws InvalidPositionException if p !valid
        @throws RemovalException if node has children
      */
    public T removeAt(Position<T> p)
        throws InvalidPositionException, RemovalException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        if (n.children != null) {
            throw new RemovalException();
        }
        T t = n.data;
        // if position given coordinates to root,
        // set root to null
        if (n == this.root) {
            this.root = null;
        } else { // else just remove from ArrayList
            n.parent.children.remove(n);
        }
        n.color = null;
        this.numNodes -= 1;
        return t;
    }
    /**.
        Get the root position of the tree
        @return position of tree's root
        @throws EmptyTreeException if tree was empty
    */
    public Position<T> root() throws EmptyTreeException {
        if (this.empty()) {
            throw new EmptyTreeException();
        }
        return this.root;
    }
    /**.
        Return the parent of the given position
        @param p position to return parent for
        @return position of p's parent
        @throws InvalidPositionException if p !valid
    */
    public Position<T> parent(Position<T> p) throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        if (n == this.root) {
            throw new InvalidPositionException();
        }
        n = n.parent;
        return n;
    }
    /**.
        Return iterable collection of children of the given position
        @param p position to return children for
        @return iterable with all children
        @throws InvalidPositionException if p is not valid
   */
    public Iterable<Position<T>> children(Position<T> p)
        throws InvalidPositionException {
        if (!(this.valid(p))) {
            throw new InvalidPositionException();
        }
        Node<T> n = this.convert(p);
        Iterable<Position<T>> iter = n.children;
        return iter;
        // return the children positions
    }
    // this class extends Operation<T> so that
    // positions() and toString() is easier to implement
    private static class Ops<T> extends Operation<T> {
        private int positionSize = 2;
        private int numPosition = 0;
        private ArrayList<Position<T>> positionList =
            new ArrayList<Position<T>>(this.positionSize);
        //override pre()
        public void pre(Position<T> p) {
            Node<T> n = (Node<T>) p;
            int i;
            if (n.children == null) {
                i = 0;
            } else {
                i = n.children.size();
            }
            if (this.numPosition == this.positionSize) {
                this.positionSize *= 2;
                this.positionList.ensureCapacity(this.positionSize);
            }
            this.positionList.add(n);
            this.numPosition++;
            for (int j = 0; j < i; j++) {
                Position<T> next = n.children.get(j);
                this.pre(next);
            }
        }
        private ArrayList<Position<T>> returnPosition() {
            return this.positionList;
        }
    }
    //obtains the ArrayList of positions
    private ArrayList<Position<T>> getList() {
        Node<T> n = this.root;
        Ops<T> operation = new Ops<T>();
        operation.pre(n);
        ArrayList<Position<T>> list = operation.returnPosition();
        return list;
    }
    /**.
        return iterable collection of all positions in tree
        @return Iterable with all positions
    */
    public Iterable<Position<T>> positions() {
        ArrayList<Position<T>> list = this.getList();
        Iterable<Position<T>> iter = list;
        // get all the positions in the Tree
        return iter;
    }
    /**.
        Return iterator of all elements stored in tree
        @return Iterator containing all elements in tree
    */
    public Iterator<T> iterator() {
        // It appears getting the iterator is O(n^2) time?
        ArrayList<Position<T>> list = this.getList();
        ArrayList<T> dataList = new ArrayList<T>(list.size());
        for (int i = 0; i < list.size(); i++) {
            Position<T> p = list.get(i);
            Node<T> m = this.convert(p);
            dataList.add(m.data);
        }
        // get the positions then make an iterator
        // which points to the data fields
        Iterator<T> iterator = dataList.iterator();
        return iterator;
    }
    /**.
        Traverse the tree performing the given oeprations required
        @param o the operation
    */
    public void traverse(Operation<T> o) {
        this.recurse(this.root, o);
    }
    private void recurse(Position<T> p, Operation<T> o) {
        if (p == null) {
            return;
        }
        int numIn = 0;
        Node<T> n = this.convert(p);
        final int maxIn = n.children.size() - 1;
        o.pre(n);
        for (int i = 0; i < n.children.size(); i++) {
            Position<T> next = n.children.get(i);
            /* Idea here is that if there are 0 children,
            do not do any in() traversals. If there are
            n children, do n - 1 traversals. Don't really
            know if I coded it right though......
            */
            if (numIn != maxIn || (numIn == 0 && maxIn == 0)) {
                o.in(next);
                numIn++;
            }
            this.recurse(next, o);
        }
        o.post(n);
    }
    /**.
        Returns a string containig all elements in the
        tree in the pre() ordering
        @return string containing elements
    */
    public String toString() {
        if (this.numNodes == 0) {
            return "[]";
        }
        String output = "";
        ArrayList<Position<T>> list = this.getList();
        // get the ArrayList, then add to string
        // data in each non-null index
        for (int i = 0; i < list.size(); i++) {
            Node<T> m = (Node<T>) list.get(i);
            output = output + ", " + m.data;
        }
        output = output.substring(2, output.length());
        return "[" + output + "]";
    }
    public static void main (String[] args) { 
        Tree<Integer> l = new TreeImplementation<Integer>();
        Position<Integer> x = l.insertRoot(50);
        Iterable<Position<Integer>> itb = l.children(x);
        if (itb != null) {
          Iterator<Position<Integer>> iter = itb.iterator();
        }
    }
}
