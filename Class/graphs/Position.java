public interface Position<T> { 
  void put(T t);
  T get();
}
