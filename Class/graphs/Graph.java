public interface Graph<V,E> { 
    Vertex<V> insertVertex(V v);
    Edge<E> insertEdge(Vertex<V> from, Vertex<V> to, E e) throws InvalidPositionException, InsertionException;
    
    V removeVertex(Vertex<V> p) throws InvalidPositionException, RemovalException;
    E removeEdge(Edge<E> p) throws InvalidPositionException;
  
    Iterable<Vertex<V>> vertices;
    Iterable<Edge<E>> vertices;

    Iterable<Edge<E>> incoming(Vertex<V> p) throws InvalidPositionException;
    Iterable<Edge<E>> outgoing(Vertex<V> p) throws InvalidPositionException;

    Vertex<V> start(Edge<E> e) throws InvalidPositionException;
    Vertex<V> end(Edge<E> e) throws InvalidPositionException;
}

// vertex and edges are subtypes of position
