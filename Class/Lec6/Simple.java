import org.junit.Test;
import static org.junit.Assert.assertEquals;

//#assignments
//#comparisons


public class Simple
{
//Linear Search
  public int find(int a[], int k)
  {
    for(int i = 0; i < a.length; i++)
    {
      if (a[i] == k)
        return i;
    }
      return -1;
  }

  @Test
  public void notFoundReturnsBad()
  {
    int a[] = {10, 20, 30, 40};
    assertEquals(-1,find(a,31));
    assertEquals(-1,find(a,1));
    assertEquals(-1,find(a,41));;
    assertEquals(0,find(a,10));
    assertEquals(1,find(a,20));
   
  }

  @Test
  public void nullArray()
  {
    int a[] = new int[0];
    assertEquals(-1,find(a,0));
  }

}

/* O(n): set of all functions that grow
  roughly by n */
