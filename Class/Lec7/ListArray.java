//<T> is generics 
public class ListArray<T> implements Array<T>
{

  private static class Node<T> //Nested class
  {
    Node<T> next;
    T data;

  }

  private int length;
  private Node<T> rep;

  private void prepend(Node<T> n)
  {
    n.next = this.rep;
    this.rep = n;

  }

  private Node<T> find(int index) throws IndexException
  {
    if (index < 0 || index >= this.length)
      throw new IndexException();

    Node<T>n = rep;
    while (index > 0) 
    {
      n = n.next;
      index--;
    }

    return n;

  }

  public ListArray(int length, T t) throws LengthException
  {
    if (length <= 0)
      throw new LengthException();

    this.length = length;

    for (int i = 0; i < this.length; i++)
    {
      Node<T> n = new Node<T>();
      n.data = t;
      this.prepend(n);
    }    
  }

  public int length() 
  {
    return this.length;
  }

  public void set(int index, T t) throws IndexException
  {
    this.find(index).data = t;

  }
  public T get(int index) throws IndexException
  {
    return this.find(index).data;
  }

}
