import org.junit.Test;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.DataPoint;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;


@RunWith(Theories.class)
public class ArrayTestTwo
{
  //Warning: read up on anonymous classes


  private interface Fixture<T>
  {
    Array<T> init (int length, T t) throws LengthException;
  }

  @DataPoint
  public static final Fixture<String> simpleArray = new Fixture<String>() 
  {
    public Array<String> init(int length, String t) throws LengthException 
    {
      return new SimpleArray<String>(length,t);
    }
  };


  @DataPoint
  public static final Fixture<String> listArray = new Fixture<String>()
  {
    public Array<String> init(int length, String t) throws LengthException
    {
      return new ListArray<String>(length, t);
    }
  };

  @Theory
  public void lengthofNewArrayisFine(Fixture<String> f) throws LengthException
  {
    Array<String> a = f.init(4,"cs226");
    assertEquals(4,a.length()); 
  }

  @Theory
  public void contentofNewArrayIsFine(Fixture<String> f) throws IndexException,LengthException
  {
    Array<String> a = f.init(4,"cs226");
    for (int i =0; i < a.length(); i++)
      assertEquals("cs226",a.get(i));
      
  }

  @Theory
  public void setDoesNotChangeLength(Fixture<String> f) throws IndexException,LengthException
  {
    Array<String> a = f.init(4,"cs226");
    for (int i = 0; i < a.length(); i++)
    {
      a.set(i,"peter");
      assertEquals(4,a.length());
    }

  }

  @Theory @Test(expected=LengthException.class)
  public void zeroArrayLengthBoom(Fixture<String>f) throws LengthException
  {
    Array<String> a = f.init(0,"Peter");

  }
}
