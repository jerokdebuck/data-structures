import org.junit.Test;
import org.junit.experimental.theories.Theory;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.DataPoints;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;


@RunWith(Theories.class)
public class ArrayTest
{
  //Warning: read up on anonymous classes

  @DataPoints
  public static Array<String>[] data() throws LengthException
  {
    return new Array[]
    {
      new SimpleArray<String>(4,"cs226"),
      new ListArray<String>(4,"cs226"),
    };


  }

  @Theory
  public void lengthofNewArrayisFine(Array<String> a)
  {
    assertEquals(4,a.length()); 
  }

  @Theory
  public void contentofNewArrayIsFine(Array<String> a) throws IndexException
  {
    for (int i =0; i < a.length(); i++)
      assertEquals("cs226",a.get(i));
      
  }

  @Theory
  public void setDoesNotChangeLength(Array<String> a) throws IndexException
  {
    for (int i = 0; i < a.length(); i++)
    {
      a.set(i,"peter");
      assertEquals(4,a.length());
    }

  }

}
