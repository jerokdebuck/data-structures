//<T> is generics 
public interface Array<T>
{
  int length();
  void set(int index, T t) throws IndexException;
  T get(int index) throws IndexException; 

}
