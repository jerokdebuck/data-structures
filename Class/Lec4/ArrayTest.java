
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ArrayTest{

@Test
public void lengthOfNewArray() throws LengthException
{
  Array<String> a = new SimpleArray<String>(4,"cs226");
  assertEquals(4,a.length());
}

}
