
public class ObviousCounter implements Counter
{
  private int val;

  ObviousCounter() {this.val = 0;}
  ObviousCounter(int val) {this.val = 0;}

  public int getValue()
  {
    return this.val; 
  }

  public void increment()
  {
    this.val += 1;
  }

  public static void main(String[] args)
  {
    Counter obvious = new ObviousCounter();
    assert obvious.getValue() == 0;
    obvious.increment();
    assert obvious.getValue() == 1;
  }

}
