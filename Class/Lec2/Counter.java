
public interface Counter
{
  public int getValue();
  public void increment();
}
