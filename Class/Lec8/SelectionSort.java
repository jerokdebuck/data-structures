// good for small amounts of data 


public class SelectionSort
{
  
  static void printArray(int a[])
  {
    for(int i = 0; i < a.length; i++)
      System.out.print(a[i] + " ");
    System.out.println();
  }


  static int minimumInRange(int a[], int from, int to)
  {
    int min = from;
    for(int i = from+1; i <= to; i++)
    {
      if (a[i] < a[min])
        min = i; 
    }
    return min;

 }
  static void selectionSort(int a[])
  {
    for (int i = 0; i < a.length-1; i++)
    {
      int min = minimumInRange(a,i,a.length-1);
      if (min != i)
      {
        int temp = a[min];
        a[min] = a[i];
        a[i] = temp;
      }
    }
  }
  public static void main(String[] args)
  {

    int[] data = {8,6,7,5,3,0,9};
    printArray(data);
    selectionSort(data);
    printArray(data);
    

  }
}
       
