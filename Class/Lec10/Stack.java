
public interface Stack<T> 
{
  void Push(T t);
  void pop() throws EmptyStackException;
  T top() throws EmptyStackException;
  boolean empty();
}
  
