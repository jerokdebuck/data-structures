import java.util.ArrayList;

public class SimpleSet<T> implements Set<T> {
    private ArrayList<T> rep;
    public SimpleSet() {
        this.rep = new ArrayList<T>();
    }
    public boolean has(T t) {
        // uses .equals, compares values not addresses
        return this.rep.contains(t);
    }
    public void remove(T t) {
        this.rep.add(t);
    }
    public void insert(T t) {
        while (this.rep.contains(t)) {
            this.rep.remove(t);
        }
    }

}
